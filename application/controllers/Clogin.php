<?php

/**
* 
*/
class Clogin extends CI_Controller
{
	
		function __construct(){
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->model(array('Mpendaftaran'));

		}
		function indexlama(){
			$this->load->view('Vdaftar');
		}
		function daftar(){
			$data['nama']=$this->input->post('nama');
			$data['email']=$this->input->post('email');
			$data['password']=$this->input->post('password');
			//SET DEFAUL LEVEL NILAI 0
			$data['level']='0';
			$this->Mpendaftaran->simpandatauser($data);
			redirect(site_url('Clogin/login'));
		}
		function index(){
			$data['statuslogin']='';
			$this->load->view('user/Vloginuser',$data);
		}
		function proseslogin2(){
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('password','Password','required');
	
			if($this->form_validation->run()==true){
				echo "jalan";
			}else{
				//echo "heloo salah";
				//redirect(site_url('Clogin'));
				$data['statuslogin']='';
				$this->load->view('user/Vloginuser',$data);
			}
		}				
		function proseslogin(){
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('password','Password','required');
			
			if($this->form_validation->run()==true){
					$username=$this->input->post('email');
					$password=$this->input->post('password');
					//echo $password .$username;
					$cek=$this->Mpendaftaran->cekuser($username,$password);
					if($cek->num_rows() == 1 ){
						foreach($cek->result() as $data){
							$sess_data['id']=$data->id;
							$sess_data['nama']=$data->nama;
							$sess_data['username']=$data->email;
							$sess_data['password']=$data->password;
							$sess_data['level']=$data->level;
							$sess_data['status']="login";
							$this->session->set_userdata($sess_data);
						}
						// CEK STATUS LEVEL USER
						//DEFAULT LEVEL 0
						if($this->session->userdata('level')=='1'){
						  redirect(site_url("Coperator"));	
						}else{
						  redirect(site_url("Cuser/dashboard"));
						}
					}else{
						//echo "username dan password salah";
						$this->session->set_flashdata('message',true);
						//redirect(site_url('Clogin'));
						// $this->load->view('user/Vloginuser',$data);
						$this->load->view('user/Vloginuser');
					}
			}else{
				$this->load->view('user/Vloginuser');
			}
			
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(site_url('Clogin/login'));
		}		
}
?>