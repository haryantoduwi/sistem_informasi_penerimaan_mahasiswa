<?php
	Class Pendaftaran extends CI_Controller
	{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mdp3','mpegawai','Mdaftarkota','Mpendaftaran','Mpenjualan'));
			/*
			if($this->session->userdata('status') != "login"){
				redirect(base_url('clogin'));
			}
			*/	
		}
		function index(){
			$this->load->view('gui/header');
			$this->load->view('pendaftaran/body');
			$this->load->view('gui/footer');
		}
		function formulir(){

			//$data['provinsi']=$this->Mdaftarkota->get_kota('provinces')->result();
			//$data['kabupaten']=$this->Mdaftarkota->get_kota('regencies')->result();
			//$data['kecamatan']=$this->Mdaftarkota->get_kota('districts')->result();
			//CAPTCHA
			$this->load->helper('captcha');
			$vals = array(
                'img_path'	 => './captcha/',
                'img_url'	 => base_url().'captcha/',
                'img_width'	 => '200',
                'img_height' => 30,
                'border' => 0, 
                'expiration' => 7200	
				);
			$cap= create_captcha($vals);
			$data['image']=$cap['image'];
			//$sess_data['mycaptcha']=$cap['word'];
			$this->session->set_userdata('mycaptcha',$cap['word']);
			$this->session->set_userdata('status',1);
			$data['jurusan']=$this->Mpendaftaran->get_jurusan()->result();
			if($this->session->userdata('salahcaptcha')==true){
				$data['pesan']="Captcha Salah";

			}else{
				$data['pesan']="";

			}
			$this->load->view('gui/header');
			$this->load->view('pendaftaran/formulir',$data);
			$this->load->view('gui/footer');
		}
		function notifikasi($dt=''){
			$data['statuskirim']=$dt;
			$this->load->view('gui/header');
			$this->load->view('pendaftaran/notifikasiemail',$data);
			$this->load->view('gui/footer');			
		}
		function simpandatapendaftaran(){
			$data=$this->input->post('email');
		}

		// function submitbkb(){
		// 	$data['nama']=$this->input->post('nama');
		// 	$data['no_hp']=$this->input->post('nohp');
		// 	$data['email']=$this->input->post('email');
		// 	$data['kode_jurusan']=$this->input->post('idjurusan');
		// 	$idjurusan=$this->input->post('idjurusan');
		// 	$data['password']=$this->Mpendaftaran->gen_password(8);
		// 	$data['no_registrasi']=$this->Mpendaftaran->no_registrasi($idjurusan);
		// 	$data['tgl_daftar']=date('Y-m-d');
		// 	$data['aktif']='0';
		// 	//Menyimpandata kedatabase
		// 	$id=$this->Mpendaftaran->simpan($data);
		// 	//enkripsi ID
		// 	$encrypt_id=$id;

		// 	//SET EMAIL
		// 	$ci = get_instance();
	 //        //$config['charset']= 'utf-8';
	 //        $config['useragent']='Codeigniter';
	 //        $config['protocol'] = "smtp";
	 //        //$config['mailtype']="html";
	 //        $config['smtp_host'] = "ssl://smtp.gmail.com";
	 //        $config['smtp_port'] = "465";
	 //        $config['smtp_timeout']="400";
	 //        $config['smtp_user'] = "reactiveids@gmail.com";
	 //        $config['smtp_pass'] = "z3r0nul3";
	 //        $config['charset'] = "utf-8";
	 //        $config['mailtype'] = "html";
	 //        $config['newline'] = "\r\n";
	 //        $config['crlf'] = "\r\n";
	 //        $config['wordwrap']=TRUE;

	 //        //SET LIBRARY EMAIL CI
	 //        $ci->email->initialize($config);
		// 	/*
		// 	ob_start();
		// 	$this->load->view('pendaftaran/bodyemail',$data);
		// 	$html=ob_get_contents();
		// 	ob_end_clean();				 
	 //        */
	 //        $ci->email->from('reactiveids@gmail.com', 'penmaruakprind');
	 //        $list = array($data['email']);
	 //        $ci->email->to($list);
	 //        $ci->email->subject('PENMARU IST AKPRIND');
	 //        $ci->email->message(
	 //        	"Terimakasih telah melakukan registrasi, untuk memverifikasi silahkan klik link dibawah ini <br><br>"
	 //        	.site_url("Pendaftaran/verification/$encrypt_id")
	 //        	);
	 //        if ($this->email->send()) {
	 //            echo 'Email berhasil dikirim silahkan cek email kamu';
	 //        } else {
	 //            show_error($this->email->print_debugger());
	 //        }
		// }
		function submit2(){
			$this->form_validation->set_rules('nama','nama','required');
			$this->form_validation->set_rules('nohp','Nomor','required');
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('idjurusan','Jurusan','required');
			$this->form_validation->set_rules('captcha','Captcha','required');	
			if($this->form_validation->run()==true){
				echo "jalan";
				//ERROR dan VALUE
				$pesanerror=array('errornama','errornohp','erroremail','erroridjurusan','errorcaptcha',
					'valnama','valnohp','valemail','valemail','validjurusan',
					);
				$this->session->unset_userdata($pesanerror);
			}else{
				//echo "heloo salah";
				$errorpesan=array(
					'errornama'=>form_error('nama','<p class="field_error text-red">','</p>'),
					'errornohp'=>form_error('nohp','<p class="field_error text-red">','</p>'),
					'erroremail'=>form_error('email','<p class="field_error text-red">','</p>'),
					'erroridjurusan'=>form_error('idjurusan','<p class="field_error text-red">','</p>'),
					'errorcaptcha'=>form_error('captcha','<p class="field_error text-red">','</p>'),
					//VALUE
					'valnama'=>set_value('nama'),
					'valnohp'=>set_value('nohp'),
					'valemail'=>set_value('email'),
					'validjurusan'=>set_value('idjurusan'),
					);
				$this->session->set_userdata($errorpesan);
				redirect(site_url('Pendaftaran/formulir'));
			}
		}

		function submit(){
			//SET VALIDATION
			$this->form_validation->set_rules('nama','nama','required');
			$this->form_validation->set_rules('nohp','Nomor','required');
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('idjurusan','Jurusan','required');
			$this->form_validation->set_rules('captcha','Captcha','required');


			if($this->form_validation->run()==TRUE){
					//UNSEET SESSION ERROR dan VALUE 
					$pesanerror=array('errornama','errornohp','erroremail','erroridjurusan','errorcaptcha',
						);
					$this->session->unset_userdata($pesanerror);
					//TANGKAP INPUTAN			
					$data['nama']=$this->input->post('nama');
					$data['no_hp']=$this->input->post('nohp');
					$data['email']=$this->input->post('email');
					$data['kode_jurusan']=$this->input->post('idjurusan');
					$idjurusan=$this->input->post('idjurusan');
					$data['password']=$this->Mpendaftaran->gen_password(8);
					$data['no_registrasi']=$this->Mpendaftaran->no_registrasi($idjurusan);
					$data['tgl_daftar']=date('Y-m-d');
					$data['aktif']='0';
					
					if($this->input->post('captcha')== $this->session->userdata('mycaptcha')){
							//MENGHAPUS SESSION LASTVALUE
							$lastvalue=array(
									'valnama','valnohp','valemail','valemail','validjurusan',
								);
							$this->session->unset_userdata($lastvalue);
							//MENGHAPUS SESSION SALAH CAPTCHA
							$this->session->unset_userdata('salahcaptcha');
							//EMAIL BEGIN
							
							//Menyimpandata kedatabase
							$id=$this->Mpendaftaran->simpan($data);
							//enkripsi ID
							$encrypt_id=md5($id);
							//Mengambil Nama Jurusan
							$idjurusan=$this->input->post('idjurusan');
							$data['namajurusan']=$this->Mpendaftaran->get_nama_jurusan($idjurusan)->row();

							//SET EMAIL
							$ci = get_instance();
					        //$config['charset']= 'utf-8';
					        $config['useragent']='Codeigniter';
					        $config['protocol'] = "smtp";
					        //$config['mailtype']="html";
					        $config['smtp_host'] = "ssl://smtp.gmail.com";
					        $config['smtp_port'] = "465";
					        $config['smtp_timeout']="400";
					        $config['smtp_user'] = "reactiveids@gmail.com";
					        $config['smtp_pass'] = "z3r0nul3";
					        $config['charset'] = "utf-8";
					        $config['mailtype'] = "html";
					        $config['newline'] = "\r\n";
					        $config['crlf'] = "\r\n";
					        $config['wordwrap']=TRUE;

					        //SET LIBRARY EMAIL CI
					        $ci->email->initialize($config);
							/*
							ob_start();
							$this->load->view('pendaftaran/bodyemail',$data);
							$html=ob_get_contents();
							ob_end_clean();				 
					        */
					        $ci->email->from('reactiveids@gmail.com', 'penmaruakprind');
					        $list = array($data['email']);
					        $ci->email->to($list);
					        $ci->email->subject('PENMARU IST AKPRIND');
					        //TABEL DIRUBAH
					        $ci->email->message(
					        	"
				              <dl>
				              	<dt><b>Pilihan Prodi<b></dt>
				                <dd>".$data['namajurusan']->nama."/".$data['namajurusan']->program
				                ."</dd>
				                <dt><b>Nomor Registrasi<b></dt>
				                <dd>".$data['no_registrasi']."</dd> 
				                <dt><b>Tanggal Mendaftar<b></dt>
				                <dd>".$data['tgl_daftar']."</dd>               
				                <dt><b>Nama<b></dt>
				                <dd>".ucwords($data['nama'])."</dd>
				                <dt><b>Email<b></dt>
				                <dd>".$data['email']."</dd>
				                <dt><b>Password<b></dt>
				                <dd>".$data['password']."</dd>
				                <dt><b>No. Handphone<b></dt>
				                <dd>".$data['no_hp']."</dd>                                
				              <br><br>
				              "."Terimakasih telah melakukan registrasi, untuk memverifikasi silahkan klik link dibawah ini dengan password yang telah diberikan<br>
								Panitia Penmaru           
				              <br><br>"
					        	.site_url("Pendaftaran/verification/$encrypt_id")
					        	);
							// CEK PENGIRIMAN EMAIL
					        if ($this->email->send()) {
					        	$sess_data['salahcaptcha']=false;
								$this->session->set_userdata($sess_data);	
					        	$data="sukses";
					            redirect(site_url('Pendaftaran/notifikasi/'.$data));
					            //echo 'Email berhasil dikirim silahkan cek email kamu';
					        } else {
					            //show_error($this->email->print_debugger());
					            $sess_data=array(
					            	'nama'=>$this->input->post('nama'),
					            	'idjurusan'=>$this->input->post('idjurusan'),	
					            	);
					            $this->session->set_userdata($sess_data);
					            $data="gagal";
					        	redirect(site_url('Pendaftaran/notifikasi/'.$data));
					        }	
					        //END OF EMAIL								
									
					}else{
						//CAPTCHA SALAH
						$this->session->set_userdata($lastvalue);						
						$sess_data['salahcaptcha']=true;
						$this->session->set_userdata($sess_data);			
						redirect(base_url('Pendaftaran/formulir/'));
						
					}
			//VALIDATION TIDAK JALAN 
			}else{
				$errorpesan=array(
					'errornama'=>form_error('nama','<p class="field_error text-red">','</p>'),
					'errornohp'=>form_error('nohp','<p class="field_error text-red">','</p>'),
					'erroremail'=>form_error('email','<p class="field_error text-red">','</p>'),
					'erroridjurusan'=>form_error('idjurusan','<p class="field_error text-red">','</p>'),
					'errorcaptcha'=>form_error('captcha','<p class="field_error text-red">','</p>'),
					);
				$this->session->set_userdata($errorpesan);				
				// Redirect ke halaman utama
				redirect(base_url('Pendaftaran/formulir'));
				//$this->session->unset_userdata($salahcaptcha);
			}
			//END OF CAPTCHA  

		}
		function verification($key){
			$data=array('headline'=>'AKTIVASI PENGGUNA');
			$this->Mpendaftaran->changeActiveState($key);
			$this->load->view('gui/header');
			$this->load->view('pendaftaran/halamanaktivasi',$data);
			$this->load->view('gui/footer');	
			//echo "selamat kamu telah memverifikasi akun kamu";
			//echo "<br><br><a href='".site_url("Clogin")."'>Kembali ke menu login</a>";
		}
		// function notifikasi(){
		// 	$data['nama']=$this->input->post('nama');
		// 	$data['no_hp']=$this->input->post('nohp');
		// 	$data['email']=$this->input->post('email');
		// 	$data['kode_jurusan']=$this->input->post('idjurusan');
		// 	$idjurusan=$this->input->post('idjurusan');
		// 	$data['password']=$this->Mpendaftaran->gen_password(8);
		// 	$data['no_registrasi']=$this->Mpendaftaran->no_registrasi($idjurusan);
		// 	$data['tgl_daftar']=date('Y-m-d');
		// 	$this->Mpendaftaran->simpan($data);

		// 	$data['namajurusan']=$this->Mpendaftaran->get_nama_jurusan($idjurusan)->row();
		// 	$this->load->view('gui/header');
		// 	$this->load->view('pendaftaran/notifikasi',$data);
		// 	$this->load->view('gui/footer');
		// 	//echo $data['no_registrasi'];
		// 	//Mengirim Email
	        
	 //        $ci = get_instance();
	 //        $config['protocol'] = "smtp";
	 //        $config['smtp_host'] = "ssl://smtp.gmail.com";
	 //        $config['smtp_port'] = "465";
	 //        $config['smtp_user'] = "reactiveids@gmail.com";
	 //        $config['smtp_pass'] = "z3r0nul3";
	 //        $config['charset'] = "utf-8";
	 //        $config['mailtype'] = "html";
	 //        $config['newline'] = "\r\n";
	     	
	 //        $ci->email->initialize($config);
		// 	ob_start();
		// 	$this->load->view('pendaftaran/bodyemail',$data);
		// 	$html=ob_get_contents();
		// 	ob_end_clean();				 
	 //        $ci->email->from('reactiveids@gmail.com', 'penmaruakprind');
	 //        $list = array($data['email']);
	 //        $ci->email->to($list);
	 //        $ci->email->subject('PENMARU IST AKPRIND');
	 //        $ci->email->message($html);
	 //        if ($this->email->send()) {
	 //            //echo 'Email sent.';
	 //        } else {
	 //            show_error($this->email->print_debugger());
	 //        }
			
		// }
		function chart(){
			$data['penjualan']=$this->Mpenjualan->penjualan()->result();
			$this->load->view('gui/header');
			$this->load->view('pendaftaran/chart',$data);
			$this->load->view('gui/footer');			
		}

	}
?>