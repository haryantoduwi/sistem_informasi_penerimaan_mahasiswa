<?php
	/**
	* FOOTER ADA DUA FOLDER GUI & MASTER
	*/
	class Cuser extends CI_Controller
	{
			
		function __construct(){
			parent::__construct();
			$this->load->model(array('Muser','Mdaftarkota'));
			if($this->session->userdata('status')!="login"){
				redirect(base_url('Clogin'));
			}

		}
		
		public $cont="Cuser";
	
		function index(){
			$this->load->view('Vdaftar');
		}
		function daftar(){
			$data['nama']=$this->input->post('nama');
			$data['email']=$this->input->post('email');
			$data['password']=$this->input->post('password');
			//SET DEFAUL LEVEL NILAI 0
			$data['level']='0';
			$this->Muser->simpandatauser($data);
			redirect(base_url('Cuser/login'));
		}
		function login(){
			$data['statuslogin']='';
			$this->load->view('user/Vloginuser',$data);
		}
		function proseslogin(){
			$username=$this->input->post('email');
			$password=$this->input->post('password');
			//echo $password .$username;
			$cek=$this->Muser->cekuser($username,$password);
			if($cek->num_rows() == 1 ){
				foreach($cek->result() as $data){
					$sess_data['id']=$data->id;
					$sess_data['nama']=$data->nama;
					$sess_data['username']=$data->email;
					$sess_data['password']=$data->password;
					$sess_data['level']=$data->level;
					$sess_data['status']="login";
					$this->session->set_userdata($sess_data);
				}
				//DEFAULT LEVEL 0
				if($this->session->userdata('level')=='1'){
				  redirect(base_url("Coperator"));	
				}else{
					//echo "login";
				  redirect(base_url("Cuser/dashboard"));
				}
			}else{
				//echo "username dan password salah";
				$data['statuslogin']='0';
				//echo "hello ";
				//redirect(base_url('clogin',$data));
				$this->load->view('user/Vloginuser',$data);

			}			
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(base_url('Cuser'));
		}		
		function dashboard(){
			$data['controller']=$this->cont;
			$id=$this->session->userdata('id');
			$cek=$this->Muser->genall($id);
			//GET FILE FOTO
			$data['dp']=$this->Muser->getfilefoto($id)->row();	
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php',$data);
			$this->load->view('master/sidebar.php',$data);
			//$this->load->view('master/maincontent.php',$data);
			if($cek->num_rows() > 0){
				$data['dt']=$this->Muser->genall($id)->row();
				$data['cekprofil']='1';
				$this->load->view('user/Vdashboard',$data);
			}else{
				$data['cekprofil']='0';
				$this->load->view('user/Vdashboardkosong',$data); 
			}			
			   
			$this->load->view('master/footer.php');
		}
		//ISI FORMULIR DATA PRIBADI
		function isiformulir(){
			// Load modal daftar kota
			$data=array(
				'provinsi'=>$this->Mdaftarkota->get_kota('provinces')->result(),
				'kabupaten'=>$this->Mdaftarkota->get_kota('regencies')->result(),
				'kecamatan'=>$this->Mdaftarkota->get_kota('villages')->result(),
			);
			$id=$this->session->userdata('id');
			//$id=$this->session->userdata('id');
			$data['controller']=$this->cont;
			$data['jurusan']=$this->Muser->bacajurusan()->result();
			$data['dp']=$this->Muser->getfilefoto($id)->row();
			$cek=$this->Muser->getdatadiri($id);	
			$data['data']=$this->Muser->getdatadiri($id)->row();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php',$data);
			$this->load->view('master/sidebar.php',$data);
			if($cek->num_rows() > 0){
				$data['dt']=$this->Muser->genall($id)->row();
				$data['cekprofil']='1';
				$this->load->view('user/Veditdatadiri',$data);
			}else{
				$data['cekprofil']='0';
				$this->load->view('user/Visiformulir.php',$data); 
			}			
			$this->load->view('master/footer.php');
		}
		function ambil_data(){
			$modul=$this->input->post('modul');
			$id=$this->input->post('id');

			if($modul=="kabupaten"){
				echo $this->Mdaftarkota->kabupaten($id);
			}else if($modul=="kecamatan"){
				echo $this->Mdaftarkota->kecamatan($id);
			}else if($modul=="kelurahan"){

			}

		}
		//SIMPAN DATA PRIBADI
		function simpandatapribadi(){
			//Id user diambil dari session login
			$data['iduser']=$this->session->userdata('id');
			$data['idjurusan']=$this->input->post('idjurusan');
			$data['nisn']=$this->input->post('nisn');
			$data['tgllahir']=date('Y-m-d',strtotime($this->input->post('tgllahir')));
			$data['noktp']=$this->input->post('noktp');
			$data['jeniskelamin']=$this->input->post('jeniskelamin');
			$data['kewarganegaraan']=$this->input->post('kewarganegaraan');
			$data['agama']=$this->input->post('agama');
			$data['alamat']=$this->input->post('alamatasal');
			$data['kecamatan']=$this->input->post('kecamatan');
			$data['kabupaten']=$this->input->post('kabupaten');
			$data['propinsi']=$this->input->post('propinsi');
			$data['nohandphone']=$this->input->post('nohp');
			$data['kodepos']=$this->input->post('kodepos');
			$data['tgldaftar']=date('Y-m-d');
			$data['statusvalidasi']="0";
			$data['kuisioner']=$this->input->post('kuisioner');
			
			
			//UPLOAD FILERAPORT
			$filefoto=$_FILES['filefoto'];
			$data['filefoto']=$filefoto['name'];
			$config['upload_path']='./fileupload/foto';
			$config['allowed_types']='jpg|jpeg';
			$config['max_size']=5000;
			$config['file_name']=$filefoto['name'];
			$config['overwrite']=true;
			$this->load->library('upload',$config);	
				
				if(!$this->upload->do_upload('filefoto')){
					$data=array('errorfoto'=> $this->upload->display_errors(),'sukses'=> 'File berhasil diupload','statusuploadraport'=>'0');
					$data['controller']=$this->cont;
					$this->load->view('master/header.php');
					$this->load->view('master/mainheader.php',$data);
					$this->load->view('master/sidebar.php',$data);
					$this->load->view('user/Visiformulir.php',$data);
					$this->load->view('master/footer.php');						
				}else{
					echo "File Upload <br>";
				}
			//echo $config['file_name'];
			$this->Muser->simpandatapribadi($data);
			redirect(site_url('Cuser/dashboard'));
		}
		function updatedatapribadi(){
			$id=$this->input->post('id');
			$data['idjurusan']=$this->input->post('idjurusan');
			$data['nisn']=$this->input->post('nisn');
			$data['tgllahir']=date('Y-m-d',strtotime($this->input->post('tgllahir')));
			$data['noktp']=$this->input->post('noktp');
			$data['jeniskelamin']=$this->input->post('jeniskelamin');
			$data['kewarganegaraan']=$this->input->post('kewarganegaraan');
			$data['agama']=$this->input->post('agama');
			$data['alamat']=$this->input->post('alamatasal');
			$data['kecamatan']=$this->input->post('kecamatan');
			$data['kabupaten']=$this->input->post('kabupaten');
			$data['propinsi']=$this->input->post('propinsi');
			$data['nohandphone']=$this->input->post('nohp');
			$data['kodepos']=$this->input->post('kodepos');
			$data['tgldaftar']=date('Y-m-d');
			$data['statusvalidasi']="0";
			$data['kuisioner']=$this->input->post('kuisioner');
			
			
			//UPLOAD FILERAPORT
			$filefoto=$_FILES['filefoto'];
			$data['filefoto']=$filefoto['name'];
			$config['upload_path']='./fileupload/foto';
			$config['allowed_types']='jpg|jpeg';
			$config['max_size']=5000;
			$config['file_name']=$filefoto['name'];
			$config['overwrite']=true;
			$this->load->library('upload',$config);	
				
				if(empty($filefoto['name'])){
					$data['filefoto']=$this->input->post('filelama');
					$this->Muser->updatedatadiri($id,$data);
				}else{
					if(!$this->upload->do_upload('filefoto')){
					$data=array('errorfoto'=> $this->upload->display_errors(),'sukses'=> 'File berhasil diupload','statusuploadraport'=>'0');
					$data['controller']=$this->cont;
					$this->load->view('master/header.php');
					$this->load->view('master/mainheader.php',$data);
					$this->load->view('master/sidebar.php',$data);
					$this->load->view('user/Visiformulir.php',$data);
					$this->load->view('master/footer.php');						
					}else{
					//echo "File Upload <br>";
					$this->Muser->updatedatadiri($id,$data);
					}
				}
			redirect(base_url('Cuser/isiformulir'));	
		}
		function downloadfilefoto($file){
			$url=file_get_contents('fileupload/foto/'.$file);
			force_download($file,$url);
			//echo "string";
		}
		function downloadfileraport($filex){
			$file=str_replace("%20","_",$filex);
			$url=file_get_contents('fileupload/raport/'.$file);
			force_download($file,$url);
			//echo "string";
		}		
		function simpandatawali(){
			//Id user diambil dari session login
			$data['iduser']=$this->session->userdata('id');
			$data['namaibu']=$this->input->post('namaibu');
			$data['namabapak']=$this->input->post('namabapak');
			$data['namawali']=$this->input->post('namawali');
			$data['alamat']=$this->input->post('alamat');
			$data['kecamatan']=$this->input->post('kecamatan');
			$data['kabupaten']=$this->input->post('kabupaten');
			$data['propinsi']=$this->input->post('propinsi');
			$data['notlp']=$this->input->post('notlp');
			$data['kodepos']=$this->input->post('kodepos');
			$this->Muser->simpandatawali($data);			
		}
		function simpanberkas(){
			$fileraport=$_FILES['fileraport'];
			$filefoto=$_FILES['filefoto'];

			$data['iduser']=$this->session->userdata('id');
			$data['fileraport']=$fileraport['name'];
			$data['filefoto']=$filefoto['name'];

			//UPLOAD FILERAPORT
			$config['upload_path']='./fileupload/raport';
			$config['allowed_types']='pdf';
			$config['max_size']=5000;
			$config['file_name']=$fileraport['name'];
			$config['overwrite']=true;
			$this->load->library('upload',$config);		

				if(!$this->upload->do_upload('fileraport')){
					$data=array('error'=> $this->upload->display_errors(),'statusupload'=>false);
					$data['controller']=$this->cont;
					$this->load->view('master/header.php');
					$this->load->view('master/mainheader.php',$data);
					$this->load->view('master/sidebar.php',$data);
					$this->load->view('user/Vuploadberkas.php',$data);
					$this->load->view('master/footer.php');						
				}else{
					//echo "File UPload";	
					$this->Muser->simpandataberkas($data);
					$data['statusupload']=true;		
					//redirect(base_url('Cuser/uploadberkas',$data));
					$data['controller']=$this->cont;
					$this->load->view('master/header.php');
					$this->load->view('master/mainheader.php',$data);
					$this->load->view('master/sidebar.php',$data);
					$this->load->view('user/Vuploadberkas.php',$data);
					$this->load->view('master/footer.php');					
				}
		}
		function simpanberkasbkb(){
			$fileraport=$_FILES['fileraport'];
			//$filefoto=$_FILES['filefoto'];

			$data['iduser']=$this->session->userdata('id');
			$data['fileraport']=$fileraport['name'];
			$data['filefoto']=$filefoto['name'];

			//UPLOAD FILERAPORT
			$config['upload_path']='./fileupload/raport';
			$config['allowed_types']='pdf';
			$config['max_size']=5000;
			$config['file_name']=$fileraport['name'];
			$this->load->library('upload',$config);		

				if(!$this->upload->do_upload('fileraport')){
					$data=array('errorraport'=> $this->upload->display_errors(),'sukses'=> 'File berhasil diupload','statusuploadraport'=>'0');
					$data['controller']=$this->cont;
					$this->load->view('master/header.php');
					$this->load->view('master/mainheader.php',$data);
					$this->load->view('master/sidebar.php',$data);
					$this->load->view('user/Vuploadberkas.php',$data);
					$this->load->view('master/footer.php');						
				}else{
					echo "File UPload";
					/*
					//UPLOAD FILE FOTO
					$config['upload_path']='./fileupload/foto';
					$config['allowed_types']='jpg|jpeg';
					$config['max_size']=5000;
					$config['file_name']=$filefoto['name'];
					$this->load->library('upload',$config);
					if(!$this->upload->do_upload('filefoto')){
						$data=array('errorfoto'=> $this->upload->display_errors(),'sukses'=> 'File berhasil diupload','statusuploadfoto'=>'0');
						$data['controller']=$this->cont;
						$this->load->view('master/header.php');
						$this->load->view('master/mainheader.php',$data);
						$this->load->view('master/sidebar.php',$data);
						$this->load->view('user/Vuploadberkas.php',$data);
						$this->load->view('master/footer.php');					
					}else{
						echo "Hello wolrd";
					} 
					*/					
				}
		}
		//ISI FORMULIR DATA ORANG TUA
		function datawali(){ 
			$data['controller']=$this->cont;
			$iduser=$this->session->userdata('id');
			$cek=$this->Muser->getorangtua($iduser);
			$data['dp']=$this->Muser->getfilefoto($iduser)->row();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php',$data);
			$this->load->view('master/sidebar.php',$data);
			if($cek->num_rows() > 0){
				$data['data']=$this->Muser->getorangtua($iduser)->row();
				$data['cekprofil']='1';
				$this->load->view('user/Veditdatawali',$data);
			}else{
				$data['cekprofil']='0';
				$this->load->view('user/Vdatawali.php',$data); 
			}				
			$this->load->view('master/footer.php');			
		}
		//UPDATE DARA ORANGTUA
		function updatedatawali(){
			$id=$this->input->post('id');
			$data['namaibu']=$this->input->post('namaibu');
			$data['namabapak']=$this->input->post('namabapak');
			$data['namawali']=$this->input->post('namawali');
			$data['alamat']=$this->input->post('alamat');
			$data['kecamatan']=$this->input->post('kecamatan');
			$data['kabupaten']=$this->input->post('kabupaten');
			$data['propinsi']=$this->input->post('propinsi');
			$data['notlp']=$this->input->post('notlp');
			$data['kodepos']=$this->input->post('kodepos');
			$this->Muser->updateorangtua($id,$data);			
			redirect(base_url('Cuser/datawali'));
		}
		//ISI FORMULIR UPLOAD BERKAS
		function uploadberkas($statusupload=""){
			$data['controller']=$this->cont;
			$iduser=$this->session->userdata('id');
			//SET STATUS UPLOAD NULL
			//$data['statusuploadraport']='';
			//$data['statusuploadfoto']='';
			$data['statusupload']=$statusupload;
			$data['dp']=$this->Muser->getfilefoto($iduser)->row();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php',$data);
			$this->load->view('master/sidebar.php',$data);
			$cek=$this->Muser->getberkas($iduser);
			if($cek->num_rows() > 0){
				$data['data']=$this->Muser->getberkas($iduser)->row();
				$data['cekprofil']='1';
				$this->load->view('user/Veditberkas',$data);
			}else{
				$data['cekprofil']='0';
				$this->load->view('user/Vuploadberkas.php',$data); 
			}				
			
			$this->load->view('master/footer.php');				
		}
		function updateberkas(){
			$fileraport=$_FILES['fileraport'];
			$id=$this->input->post('id');
			$data['fileraport']=$fileraport['name'];
			$data['filefoto']="";
			//UPLOAD FILERAPORT
			$config['upload_path']='./fileupload/raport';
			$config['allowed_types']='pdf';
			$config['max_size']=5000;
			$config['file_name']=$fileraport['name'];
			$config['overwrite']=true;
			$this->load->library('upload',$config);
			
			if(!$this->upload->do_upload('fileraport')){
					$data=array('errorraport'=> $this->upload->display_errors(),'sukses'=> 'File berhasil diupload','statusuploadraport'=>'0');
					$data['controller']=$this->cont;
					$this->load->view('master/header.php');
					$this->load->view('master/mainheader.php',$data);
					$this->load->view('master/sidebar.php',$data);
					$this->load->view('user/Vuploadberkas.php',$data);
					$this->load->view('master/footer.php');						
				}else{
					//echo "File UPload";
					$this->Muser->updateberkas($id,$data);
					redirect(base_url('Cuser/uploadberkas'));
				}			
		}
	function download(){
		$data['controller']=$this->cont;
		$this->load->view('master/header.php');
		$this->load->view('master/mainheader.php',$data);
		$this->load->view('master/sidebar.php',$data);
		$this->load->view('user/Vdownload.php',$data);
		$this->load->view('master/footer.php');
	}	

	}
?>