<?php

	class Clogin extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Mlogin');
		}
		function index(){
			//SET STATUS AWAL NULL
			$data['statuslogin']='';
			$this->load->view('login',$data);
		}
		function aksi_login(){
			$username=$this->input->post('username');
			//$password=md5($this->input->post('password'));
			$password=$this->input->post('password');
			$cek=$this->Mlogin->cek($username,$password);
			if($cek->num_rows() == 1 ){
				foreach($cek->result() as $data){
					$sess_data['id']=$data->id;
					$sess_data['nama']=$data->nama;
					$sess_data['username']=$data->username;
					$sess_data['password']=$data->password;
					$sess_data['level']=$data->level;
					$sess_data['status']="login";
					$this->session->set_userdata($sess_data);
				}
				if($this->session->userdata('level')=='0'){
				  redirect(base_url("Coperator"));	
				}else{
					echo "login";
				  //redirect(base_url("cadmin"));
				}
			}else{
				//echo "username dan password salah";
				$data['statuslogin']='0';
				//redirect(base_url('clogin',$data));
				$this->load->view('login',$data);
			}
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(base_url('clogin'));
		}	
	
	}
?>