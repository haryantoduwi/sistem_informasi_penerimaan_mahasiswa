<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Mpendaftaran extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->db2=$this->load->database('dbdaftarkota',true);	
	}
	public function gen_password($panjang){
		$karakter="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
		$string="";
   			for ($i = 0; $i < $panjang; $i++) {
		 		$pos = rand(0, strlen($karakter)-1);
		  		$string .= $karakter{$pos};
		    }		
		return $string;
	}
	public function get_jurusan(){
		$query=$this->db->query("SELECT * FROM tb_jurusan");
		return $query;
	}
	public function get_nama_jurusan($id){
		$query=$this->db->query("SELECT nama,program FROM tb_jurusan WHERE id='$id'");
		return $query;
	}
	//TIDAK BISA DIPAKAI
	public function get_kode_registrasi(){
		$q=$this->db->query("SELECT MAX(RIGHT(no_registrasi,4)) AS idmax FROM tb_pendaftar");
		$kd=""; //SET KODE AWAL
		if($q->num_rows()>0){
			//JIKA DATA ADA, LEBIH DARI SATU
			foreach ($q->result() as $k){
				# code...
				$tmp =((int)$k->idmax)+1; //STRING DISET KE INT DAN DITAMBAH 
				$kd=sprintf("s",$tmp);//KODE DIAMBIL 4 KARAKTER TERAKHIR
				}	
			}else{
				$kd = "0001";
			}
		$kar="REG."; //TAMBAHAN KARAKTER
		return $kar.$kd;
	}
	function no_registrasi($idjurusan){
		$this->db->select('RIGHT(tb_pendaftar.no_registrasi,4) as kode', FALSE);
		$this->db->order_by('no_registrasi','DESC');
		$this->db->limit(1);
		$q = $this->db->get('tb_pendaftar');
        if($q->num_rows() <> 0){ //jika data ada
           $data = $q->row();
           $kd = intval($data->kode) + 1;

        }else{ //jika data kosong diset ke kode awal
            $kd = 1;
        }
	    $kodemax = str_pad($kd, 4, "0", STR_PAD_LEFT);
		$th=substr(date('Y'),2);
		$kodejadi =$th.$idjurusan.$kodemax;
        return $kodejadi;
	}	
	public function simpan($data){
		$this->db->insert('tb_calonmaba',$data);
		return $this->db->insert_id();
	}
	public function changeActiveState($key){
		$data['aktif']="1";
		$this->db->where('md5(id::text)',$key);
		$this->db->update('tb_calonmaba',$data);
		//$this->db->query("UPDATE tb_calonmaba SET aktif='1' WHERE md5(id)=$key");
		return true;
	}
	function cekuser($username,$password){
			$query=$this->db->query("SELECT * FROM tb_calonmaba WHERE email='$username' AND password='$password'");
			return $query;
	}
}
?>
