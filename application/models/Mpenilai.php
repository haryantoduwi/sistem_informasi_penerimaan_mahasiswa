<?php
	class Mpenilai extends CI_Model{

		function simpandata($data){
			//return $this->db->get('db_pegawai');
			$this->db->insert('tb_penilai',$data);
		}
		function hapusdata($id_penilai){
			$this->db->where('id_penilai',$id_penilai);
			$this->db->delete('tb_penilai');
		}
		function tampildata(){
			//return $this->db->get('db_pegawai');
			$query = $this->db->query('SELECT v_penilai.nik, v_penilai.nama, v_penilai.pangkat, v_penilai.jabatan, v_penilai.departmentname, v_atasanpenilai.nik AS nikatasanpenilai, v_atasanpenilai.nama AS namaatasanpenilai, v_penilai.id_penilai FROM (v_penilai JOIN v_atasanpenilai ON ((v_atasanpenilai.id_atasanpenilai = v_penilai.id_atasan)));');
			return $query;
		}
		function pilihid_penilai($id_penilai){
			$this->db->select('*');
			$this->db->from('db_user');
			$this->db->where('id_penilai',$id_penilai);
			return $this->db->get();
		}
		function edit($id_penilai){
			$this->db->select('*');
			$this->db->from('tb_penilai');
        	$this->db->where('id_penilai',$id_penilai);
        	return $this->db->get();
       		//$query = $this->db->get();
        	//return $query->row();
		}
		function update($id_penilai, $data){
			$this->db->where('id_penilai', $id_penilai);
			$this->db->update('tb_penilai', $data);
		}
		function getnamaunit(){
			$query=$this->db->query('SELECT tb_department.departmentcode, tb_department.departmentname FROM tb_department ');
			return $query;
		}
		function getdatapegawai(){
			$query=$this->db->query('SELECT * FROM tb_employee order by employeelastname asc');
			return $query;
		}
		function getdataatasan(){
			$query=$this->db->query('SELECT v_atasanpenilai.nik, v_atasanpenilai.nama, v_atasanpenilai.id_atasanpenilai FROM v_atasanpenilai;');
			return $query;
		}
	}
?>