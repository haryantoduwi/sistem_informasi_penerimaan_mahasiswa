<?php
	class Muser extends CI_Model{

		function simpandatapribadi($data){
			//return $this->db->get('db_pegawai');
			//$this->db->insert('tb_admin',$data);
			$this->db->insert('tb_datadiri',$data);
		}
		function simpandatawali($data){
			$this->db->insert('tb_orangtua',$data);
		}
		function simpandataberkas($data){
			$this->db->insert('tb_berkas',$data);
		}
		function simpandatauser($data){
			$this->db->insert('tb_user',$data);
		}
		function getdatadiri($iduser){
			$query=$this->db->query("SELECT * FROM tb_datadiri where iduser='$iduser'");
			return $query;
		}
		function updatedatadiri($id,$data){
			$this->db->where('id',$id);
			$this->db->update('tb_datadiri',$data);
		}
		function getorangtua($iduser){
			$query=$this->db->query("SELECT * FROM tb_orangtua where iduser='$iduser'");
			return $query;
		}
		function updateorangtua($id, $data){
			$this->db->where('id', $id);
			$this->db->update('tb_orangtua', $data);
		}
		function getberkas($iduser){
			$query=$this->db->query("SELECT * FROM tb_berkas where iduser='$iduser'");
			return $query;			
		}	
		function updateberkas($id,$data){
			$this->db->where('id', $id);
			$this->db->update('tb_berkas', $data); 			
		}	
		function genall($id){
			$query=$this->db->query("SELECT
			tb_user.nama,
			tb_user.id,
			tb_user.email,
			tb_user.password,
			tb_user.level,
			tb_datadiri.id AS iddatadiri,
			tb_datadiri.idjurusan,
			tb_datadiri.tgllahir,
			tb_datadiri.jeniskelamin,
			tb_datadiri.kewarganegaraan,
			tb_datadiri.agama,
			tb_datadiri.alamat,
			tb_datadiri.kecamatan,
			tb_datadiri.propinsi,
			tb_datadiri.kodepos,
			tb_datadiri.nohandphone,
			tb_datadiri.nisn,
			tb_datadiri.kabupaten,
			tb_datadiri.tgldaftar,
			tb_datadiri.statusvalidasi,
			tb_datadiri.notest,
			tb_datadiri.noktp,
			tb_datadiri.kuisioner,
			tb_datadiri.filefoto,
			tb_orangtua.id AS idorangtua,
			tb_orangtua.namaibu,
			tb_orangtua.namabapak,
			tb_orangtua.alamat AS alamatortu,
			tb_orangtua.kecamatan AS kecamatanortu,
			tb_orangtua.propinsi AS propinsiortu,
			tb_orangtua.kodepos AS kodeposortu,
			tb_orangtua.notlp AS notlportu,
			tb_orangtua.kabupaten AS kabupatenortu,
			tb_orangtua.namawali,
			tb_berkas.fileraport
		FROM
			tb_datadiri
		INNER JOIN tb_user ON tb_datadiri.iduser = tb_user.id
		INNER JOIN tb_orangtua ON tb_orangtua.iduser = tb_user.id
		INNER JOIN tb_berkas ON tb_berkas.iduser = tb_user.id
		WHERE
			tb_user.id = '$id'");
		return $query;
		}
		function getfilefoto($id){
			$query=$this->db->query("SELECT
					tb_user.id AS iduser,
					tb_datadiri.id AS iddatadiri,
					tb_datadiri.filefoto
					FROM
					tb_user
					INNER JOIN tb_datadiri ON tb_datadiri.iduser = tb_user.id
					WHERE
					tb_user.id = '$id'
				");
			return $query;
		}
		function cekuser($username,$password){
			$query=$this->db->query("SELECT * FROM tb_user WHERE email='$username' AND password='$password'");
			return $query;
		}
		function bacajurusan(){
			$query=$this->db->query("SELECT * FROM tb_jurusan");
			return $query;
		}
		//FUNGSI KEBAWAH TIDAK DIPAKAI
		function hapusdatauser($iduser){
			$this->db->where('id',$iduser);
			$this->db->delete('tb_admin');
		}
		function tampildatauser(){
			//return $this->db->get('db_pegawai');
			$query = $this->db->query('SELECT tb_admin.username, tb_admin.password, tb_admin.id, tb_penilai.nik AS nikpenilai, tb_penilai.nama AS penilai, tb_atasanpenilai.nama AS atasanpenilai, tb_atasanpenilai.nik AS nikatasanpenilai FROM ((tb_admin JOIN tb_penilai ON ((tb_admin.id_penilai = tb_penilai.id_penilai))) JOIN tb_atasanpenilai ON ((tb_penilai.id_atasan = tb_atasanpenilai.id_atasanpenilai)));');
			return $query;
		}
		function pilihid($iduser){
			$this->db->select('*');
			$this->db->from('db_user');
			$this->db->where('id_agenda',$id_agenda);
			return $this->db->get();
		}
		function edit($id){
			$this->db->select('*');
			$this->db->from('tb_admin');
        	$this->db->where('id',$id);
        	return $this->db->get();
       		//$query = $this->db->get();
        	//return $query->row();
		}
		function update($id, $data){
			$this->db->where('id', $id);
			$this->db->update('tb_admin', $data);
		}
		function getdatapenilai(){
			$query=$this->db->query('SELECT
									v_penilai.nik,
									v_penilai.nama,
									v_penilai.id_penilai,
									v_penilai.departmentname
									FROM
									v_penilai');
			return $query;
		}
	}
?>