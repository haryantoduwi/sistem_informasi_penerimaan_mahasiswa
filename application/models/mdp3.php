<?php
	class Mdp3 extends CI_Model{

		function simpandatadp3($data){
			//return $this->db->get('db_pegawai');
			$this->db->insert('tb_penilaian',$data);
		}
		function tampildatadp3($id){
			//return $this->db->get('db_pegawai');
			//$query = $this->db->query('SELECT * FROM tb_penilaian');
			//$query=$this->db->query(' SELECT tb_pegawai.nik, tb_pegawai.nama, tb_pegawai.unitkerja as unit, tb_penilaian.kesetiaan, tb_penilaian.prestasi, tb_penilaian.tanggungjwb, tb_penilaian.ketaatan, tb_penilaian.kejujuran, tb_penilaian.kerjasama, tb_penilaian.prakarsa, tb_penilaian.kepemimpinan,tb_penilaian.id_penilaian FROM (tb_penilaian JOIN tb_pegawai ON ((tb_penilaian.fingerid = tb_pegawai.fingerid)));');
			$query=$this->db->query("SELECT tb_employee.employeelastname, tb_employee.employeemiddlename, tb_employee.employeefirstname, tb_penilaian.kesetiaan, tb_penilaian.prestasi, tb_penilaian.tanggungjwb, tb_penilaian.ketaatan, tb_penilaian.kejujuran, tb_penilaian.kerjasama, tb_penilaian.prakarsa, tb_penilaian.kepemimpinan, tb_employee.departmentcode, tb_employee.employeeid,  tb_penilaian.id_penilaian FROM (tb_penilaian JOIN tb_employee ON ((tb_penilaian.fingerid = tb_employee.fingerprintid))) WHERE (tb_penilaian.idadmin ='$id')");
			return $query;
		}
			function lihatnilai($id){
			$query=$this->db->query("SELECT tb_pegawai.nama,
								    tb_penilaian.id AS id_penilaian,
								    tb_penilaian.kesetiaan,
								    tb_penilaian.prestasi,
								    tb_penilaian.tanggungjwb,
								    tb_penilaian.ketaatan,
								    tb_penilaian.kejujuran,
								    tb_penilaian.kerjasama,
								    tb_penilaian.prakarsa,
								    tb_penilaian.kepemimpinan,
								    tb_pegawai.nik,
								    tb_admin.id,
								    tb_pegawai.id AS id_pegawai,
								    tb_penilaian.periodeawal,
    								tb_penilaian.periodeakhir
								   FROM ((tb_admin
								   JOIN tb_pegawai ON ((tb_pegawai.idadmin = tb_admin.id)))
								   JOIN tb_penilaian ON ((tb_penilaian.idpegawai = tb_pegawai.id)))
								  WHERE (tb_admin.id = '$id');");
			return $query;
		}
		 
		function getdatapegawai(){
				$this->db->select('*');
				//$this->db->from('tb_pegawai');
				$this->db->from('tb_employee');
				$this->db->order_by("employeelastname", "asc");
				return $this->db->get();
		}
		
		function hapusdatadp3($iddp3){
			$this->db->where('id_penilaian',$iddp3);
			$this->db->delete('tb_penilaian');
		}
		function getdatanilai($idnilai){
			$query=$this->db->query("SELECT * FROM v_lihatnilai where id_penilaian='$idnilai'");
			return $query;
		}
		/*
		function pilihid($iddp3){
			$this->db->select('*');
			$this->db->from('db_dp3');
			$this->db->where('id_agenda',$id_agenda);
			return $this->db->get();
		}
		function edit($id){
			$this->db->select('*');
			$this->db->from('tb_admin');
        	$this->db->where('id',$id);
        	return $this->db->get();
       		//$query = $this->db->get();
        	//return $query->row();
		}
		function update($id, $data){
			$this->db->where('id', $id);
			$this->db->update('tb_admin', $data);
		}
		*/
	}
?>