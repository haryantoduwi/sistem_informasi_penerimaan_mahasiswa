<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Mdaftarkota extends CI_Model
{
	
	function __construct()
	{
		$this->dbdaftarkota=$this->load->database('dbdaftarkota',true);	
	}
	public function get_kota($namatabel){
		$query=$this->dbdaftarkota->query("SELECT * FROM $namatabel ORDER BY name ASC");
		return $query;
	}
	public function kabupaten($provId){
		$kabupaten="<option value='0'>--Pilih--</option>";
		$this->dbdaftarkota->order_by('name','ASC');
		$kab=$this->dbdaftarkota->get_where('regencies', array('province_id'=>$provId));
		//$kab=$this->dbdaftarkota->query("SELECT * FROM regencies WHERE province_id='$provId' ORDER BY name ASC");
		 foreach ($kab->result_array() as $data) {
		 	$kabupaten.="<option value='$data[id]'>$data[name]</option>";
		 }
		 return $kabupaten;
	}
	public function kecamatan($kecId){
		$kecamatan="<option value='0'>--Pilih Kecamatan--</option>";
		$this->dbdaftarkota->order_by('name','ASC');
		$kec=$this->dbdaftarkota->get_where('districts',array('regency_id'=>$kecId));
		foreach ($kec->result_array() as $data) {
			$kecamatan.="<option value='$data[id]'>$data[name]</option>";
		}
		return $kecamatan;


	}
}
?>
