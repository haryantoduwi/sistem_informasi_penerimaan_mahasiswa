<?php
	class Matasanpenilai extends CI_Model{

		function simpandata($data){
			//return $this->db->get('db_pegawai');
			$this->db->insert('tb_atasanpenilai',$data);
		}
		function hapusdata($id_atasanpenilai){
			$this->db->where('id_atasanpenilai',$id_atasanpenilai);
			$this->db->delete('tb_atasanpenilai');
		}
		function tampildata(){
			//return $this->db->get('db_pegawai');
			$query = $this->db->query('SELECT tb_atasanpenilai.nik, tb_atasanpenilai.nama, tb_atasanpenilai.id_atasanpenilai, tb_atasanpenilai.pangkat, tb_atasanpenilai.unitorganisasi, tb_department.departmentname FROM (tb_atasanpenilai JOIN tb_department ON (((tb_department.departmentcode)::text = (tb_atasanpenilai.id_dept)::text)));');
			return $query;
		}
		function pilihid_atasanpenilai($id_atasanpenilai){
			$this->db->select('*');
			$this->db->from('db_user');
			$this->db->where('id_atasanpenilai',$id_atasanpenilai);
			return $this->db->get();
		}
		function edit($id_atasanpenilai){
			$this->db->select('*');
			$this->db->from('tb_atasanpenilai');
        	$this->db->where('id_atasanpenilai',$id_atasanpenilai);
        	return $this->db->get();
       		//$query = $this->db->get();
        	//return $query->row();
		}
		function update($id_atasanpenilai, $data){
			$this->db->where('id_atasanpenilai', $id_atasanpenilai);
			$this->db->update('tb_atasanpenilai', $data);
		}
		function getnamaunit(){
			$query=$this->db->query('SELECT tb_department.departmentcode, tb_department.departmentname FROM tb_department ');
			return $query;
		}
		function getdatapegawai(){
			$query=$this->db->query('SELECT * FROM tb_employee order by employeelastname asc');
			return $query;
		}
	}
?>