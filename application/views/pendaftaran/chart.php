  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <!--
      <section class="content-header">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>
      -->
      <!-- Main content -->
      <section class="content">
        <div class="col-sm-12">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">Grafik Pendaftaran</h3>
	          </div>
	          <div class="box-body">
				<canvas id="areaChart" style="height:200px"></canvas>	
					                    	                                  
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>
        <div class="col-sm-12">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">Grafik Pendaftaran</h3>
	          </div>
	          <div class="box-body">
					
				<canvas id="lineChart" style="height:250px"></canvas>		                    	                                  
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->