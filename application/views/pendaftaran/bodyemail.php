        <div class="col-sm-9">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">PENGUMUMAN PENDAFTARAN ONLINE IST AKPRIND</h3>
	          </div>
	          <div class="box-body"> 
            <p>Form Pendaftaran anda telah diproses, dengan identitas sebagai berikut</p>
              <dl class="dl-horizontal">
                <dt>Nama</dt>
                <dd class="kapital"><?php echo $nama;?></dd>
                <dt>No. Pendaftran</dt>
                <dd>02154121</dd>
                <dt>Tanggal Mendaftar</dt>
                <dd><?php echo date('d-m-Y');?></dd>                                
                <dt>E-mail</dt>
                <dd><?php echo $email;?></dd>                
                <dt>Password</dt>
                <dd><b><?php echo $password;?></b></dd>                 
                <dt>Pilihan </dt>
                <dd><?php echo $namajurusan->nama ." - " .$namajurusan->program;?></dd>                
                <dt>No.Hp</dt>
                <dd><?php echo $nohp;?>
                </dd>
                </dd>
              </dl>
              <p>Silahkan melakukan login dengan menggunakan nomor pendaftaran dan password yang telah diberikan</p>                               	                                  
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>