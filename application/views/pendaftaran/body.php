  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <!--
      <section class="content-header">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>
      -->
      <!-- Main content -->
      <section class="content">
      <div class="col-sm-3">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">Menu</h3>
	          </div>
	          <div class="box-body">   
				<ul class="nav nav-pills nav-stacked">
				  <li class="active"><a href="#">Pengumuman</a></li>
				  <li><a href="<?php echo site_url('Pendaftaran/formulir');?>">Pendaftaran</a></li>
				  <li class="hide"><a href="<?php echo site_url('Pendaftaran/notifikasi');?>">Notifikasi</a></li>
				  <li class="hide"><a href="<?php echo site_url('Login');?>">Login</a></li>
				</ul>	                          
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->      	
      </div>
        <div class="col-sm-9">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">PENGUMUMAN PENDAFTARAN ONLINE IST AKPRIND</h3>
	          </div>
	          <div class="box-body">
	          	<div id="headline">
	            	<h4><b>Jalur Pendaftaran</b></h4>
	              <ol>
	                <li>Gelombang I   : 02 Januari - 31 Maret 2017</li>
	                <li>Gelombang II  : 03 April - 30 Juni 2017</li>
	                <li>Gelombang III : 03 Juli - 18 Agustus 2017</li>
	              </ol>	          		
	          	</div>	       
                <div class="checkbox">
                   <label>
                      <input id="paham" type="checkbox"> <b>Saya paham dan Lanjutkan ke Alur Pendaftaran</b>
                    </label>
                </div>
                <div id="headline2">
			        <div class="box box-success" id="box">
			          <div class="box-header with-border">
			            <h3 class="box-title">PENMARU ONLINE IST AKPRIND</h3>
			          </div>
			          <div class="box-body">
			            <div align="center">
			              <img src="./upload/img/PMBOnline.png" class="img-rounded text-center" alt="PMB Online" width="100%" height="500" > 
			            </div>
			            <br>
			            <h4><b>Alur Pendaftaran Online</b></h4>
			              <ol>
			                <li>Mengakses Situs/Web : www.akprind.ac.id</li>
			                <li>Pilih Menu Admisi/Penmaru kemudian Isi Data Form Pendaftaran</li>
			                <li>Unggah Persyaratan Bebas TES dan Surat Keterangan Dokter Bebas Narkoba dan Buta Warna (Sebagai Rekomendasi)</li>
			                <li>Jawaban Sistem Anda Diterima/Surat Panggilan + No. Pendaftaran</li>
			                <li>Transfer Pembayaran Tahap 1 Berdasarkan No . Pendaftaran</li>
			                <li>Upload File Bukti Pembayaran</li>
			                <li>Mendapatkan Kartu Mahasiswa Sementara</li>
			              </ol>                    
			          </div>         
			          <!-- /.box-body -->
			        </div>
			        <!-- /.box -->
                <div class="checkbox">
                   <label>
                      <input  id="paham2" type="checkbox"> <b>Saya paham dan Lanjutkan ke Proses Selanjutnya</b>
                    </label>
                </div>			                        	
                </div>

                <div id="headline3">
                	<a href="<?php echo site_url("Pendaftaran/formulir")?>" class="btn btn-block btn-success">Daftar</a>
                </div>                    	                                  
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->