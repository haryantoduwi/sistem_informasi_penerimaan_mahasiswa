  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <!-- Main content -->
      <section class="content">
      <div class="col-sm-3">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">Menu</h3>
	          </div>
	          <div class="box-body">   
				<ul class="nav nav-pills nav-stacked">
				  <li ><a href="<?php echo site_url('Pendaftaran');?>">Pengumuman</a></li>
				  <li class="active"><a href="#">Pendaftaran</a></li>
				  <li class="hide"><a href="<?php echo site_url('Pendaftaran/notifikasi');?>">Notifikasi</a></li>
				  <li class="hide"><a href="<?php echo site_url('Login');?>">Login</a></li>
				</ul>	                          
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->      	
      </div>
        <div class="col-sm-9">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">FORMULIR PENDAFTARAN ONLINE IST AKPRIND</h3>
	          </div>
	          <div class="box-body">
              <form role="form" action="<?php echo site_url('Pendaftaran/submit');?>" method="POST" enctype="multipart/form-data" >
                <div class="box-body">
                  <div class="form-group">
                    <label for="">Pilihan Jurusan <?php echo $this->session->userdata('validjurusan');?></label>
                    <select class="form-control select2" style="width: 100%;" name="idjurusan">
                        <?php
                        
                          foreach($jurusan as $dt){
                            //echo "<option value='".trim($dt->kode)."' Class='".if($this->session->userdata('validjurusan')==trim($dt->kode)){."selected'".;}." >".ucwords($dt->nama)." - ".ucwords($dt->program)."</option>";
                          ?>
                            <option value="<?php echo trim($dt->kode);?>" <?php if($this->session->userdata('validjurusan')==trim($dt->kode)){echo "selected=selected";}?>><?php echo ucwords($dt->nama)." - ".ucwords($dt->program)?></option>
                          <?php
                          }
                        
                        ?>
                       
                    </select>                    
                  </div>
                  <div class="form-group">
                    <label>Nama</label>
                    <input name="nama" type="text" class="form-control kapital" placeholder="Nama Lengkap" value="<?php echo $this->session->userdata('valnama');?>">
                     <p class="help-block">Sesuai Dengan Nama di Ijazah</p>
                     <?php echo $this->session->userdata('errornama');?>
                  </div>
                  <div class="form-group">
                    <label for="">No Handphone</label>
                    <input name="nohp" type="text" class="form-control"  placeholder="No Handphone" value="<?php echo $this->session->userdata('valnohp');?>">
                    <?php echo $this->session->userdata('errornohp');?>
                  </div>                  
                  <div class="form-group">
                    <label>E-mail</label>
                    <input type="text" name="email" class="form-control" placeholder="Email Aktif" value="<?php echo $this->session->userdata('valemail');?>">
                    <?php echo $this->session->userdata('erroremail');?>
                  </div>
                <div class="form-group">
                    <p><?php echo $image;?></p>  
                    <label>Captcha</label>
                    <input type="text" name="captcha" class="form-control" placeholder="Tulis Captcha"> 
                      <p class="help-block text-red" ><?php echo $pesan;?></p>
                      <?php echo $this->session->userdata('errorcaptcha');?>
                </div>                                                                                                                                                                                                                                         
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>                    	                                  
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->