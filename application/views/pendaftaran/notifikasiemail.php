  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <!--
      <section class="content-header">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>
      -->
      <!-- Main content -->
      <section class="content">
      <div class="col-sm-3">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">Menu</h3>
	          </div>
	          <div class="box-body">   
				<ul class="nav nav-pills nav-stacked">
				  <li ><a href="<?php echo site_url('Pendaftaran');?>">Pengumuman</a></li>
				  <li class="active"><a href="#">Pendaftaran</a></li>
				  <li class="hide"><a href="<?php echo site_url('Pendaftaran/notifikasi');?>">Notifikasi</a></li>
				  <li class="hide"><a href="<?php echo site_url('Login');?>">Login</a></li>
				</ul>	                          
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->      	
      </div>
        <div class="col-sm-9">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">FORMULIR PENDAFTARAN ONLINE IST AKPRIND</h3>
	          </div>
	          <div class="box-body">
              <?php if($statuskirim !="gagal"){
                  echo "
                    <div class=\"alert alert-success alert-dismissible\">
                      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        <h4><i class=\"icon fa fa-check\"></i> Sukses!</h4>
                         Email berhasil dikirim, silahkan masuk ke email anda.
                    </div>
                  ";
                  }else{
                  echo "
                    <div class=\"alert alert-danger alert-dismissible\">
                      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        <h4><i class=\"icon fa fa-check\"></i> Perhatian!</h4>
                         Email gagal dikirim.
                    </div>
                  ";
              }?>            
                   	                                  
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->