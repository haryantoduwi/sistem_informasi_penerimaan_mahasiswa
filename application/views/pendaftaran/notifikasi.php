  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <!--
      <section class="content-header">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>
      -->
      <!-- Main content -->
      <section class="content">
      <div class="col-sm-3">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">Menu</h3>
	          </div>
	          <div class="box-body">   
				<ul class="nav nav-pills nav-stacked">
				  <li  class="hide" ><a href="<?php echo base_url('Pendaftaran');?>">Pengumuman</a></li>
				  <li class="hide"><a href="<?php echo base_url('Pendaftaran/formulir');?>">Pendaftaran</a></li>
				  <li class="active"><a href="<?php echo site_url('Pendaftaran/notifikasi');?>">Notifikasi</a></li>
				  <li><a href="<?php echo site_url('Login');?>">Login</a></li>
				</ul>	                          
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->      	
      </div>
        <div class="col-sm-9">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title">PENGUMUMAN PENDAFTARAN ONLINE IST AKPRIND</h3>
	          </div>
	          <div class="box-body"> 
            <p>Form Pendaftaran anda telah diproses, dengan identitas sebagai berikut</p>
              <dl class="dl-horizontal">
                <dt>Nama</dt>
                <dd class="kapital"><?php echo $nama;?></dd>
                <dt>No. Pendaftran</dt>
                <dd>02154121</dd>
                <dt>Tanggal Mendaftar</dt>
                <dd><?php echo date('d-m-Y');?></dd>                                
                <dt>E-mail</dt>
                <dd><?php echo $email;?></dd>                
                <dt>Password</dt>
                <dd><b><?php echo $password;?></b></dd>                 
                <dt>Pilihan </dt>
                <dd><?php echo $namajurusan->nama ." - " .$namajurusan->program;?></dd>                
                <dt>No.Hp</dt>
                <dd><?php echo $no_hp;?>
                </dd>
              </dl>
              <p>Silahkan melakukan login dengan menggunakan nomor pendaftaran dan password yang telah diberikan</p>                                                                  
            </div>         
            <!-- /.box-body -->
          </div>       
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->