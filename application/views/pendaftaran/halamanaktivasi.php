  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <!--
      <section class="content-header">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>
      -->
      <!-- Main content -->
      <section class="content">
        <div class="col-sm-12">
	        <div class="box box-primary">
	          <div class="box-header with-border">
	            <h3 class="box-title"><?php echo $headline;?></h3>
	          </div>
	          <div class="box-body">    
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Berhasil!</h4>
                  Selamat kamu telah memverifikasi akun kamu.
                  <br>
                  <a href='<?php echo site_url("Clogin")?>'>Kembali ke menu login</a> 
              </div>                                                  
	          </div>         
	          <!-- /.box-body -->
	        </div>
	        <!-- /.box -->        	
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->