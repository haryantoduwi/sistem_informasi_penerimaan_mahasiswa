 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="alert alert-info alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> INFORMASI</h4>
                  Surat keterangan diterima dapat di download setelah link dibawah <b>Aktif</b> (Diaktfkan Oleh Operator)
                <br>
                Terimakasih.
            </div>          
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <label> Download Surat Undangan</label>
          <a href="#" class="btn btn-block btn-flat btn-success disabled"> Download File</a>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
