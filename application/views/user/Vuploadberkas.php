 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Upload File Berkas
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Isi Formulir</li>
      </ol>      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <div class="alert alert-info alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> PERHATIAN</h4>
                Upload File dengan format PDF untuk berkas Raport maksimal 2 Mb.
            </div>          
        </div>
      </div>
      <div class="row">
        <?php
          if($statusupload==true){
            ?>
                <div class="col-md-8 col-xs-12">
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-info"></i> PERHATIAN</h4>
                        FILE TERUPLOAD
                    </div>          
                </div>            
            <?php
          }else{
            //echo "string";
          }
        ?>        
      </div>
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Upload</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form  class="form-horizontal" role="form" action="<?php echo base_url($controller."/simpanberkas");?>" method="POST" enctype="multipart/form-data">
                <div class="box-body">                                                                                                                                                                                                                       
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="exampleInputFile">Rapor Terakhir</label>
                    <div class="col-sm-5">
                      <input type="file" name="fileraport" id="exampleInputFile">
                      <p class="help-block"><?php if(empty($error)){echo "<span class='label label-info'>File Upload Max 2Mb Format PDF</span>";}else{echo "<span class='label label-warning'>".$error."</span>";}?></p>                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">SKHUN/JIAZAH</label>
                    <div class="col-sm-5">
                      <input type="file" name="skhun/ijasah" id="skhun/ijazah">
                      <p class="help-block"><?php if(empty($error)){echo "<span class='label label-info'>File Upload Max 2Mb Format PDF</span>";}else{echo $errorfoto;}?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">KTP/KK</label>
                    <div class="col-sm-5">
                      <input type="file" name="ktp/kk" id="ktp/kk">
                      <p class="help-block"><?php if(empty($error)){echo "<span class='label label-info'>File Upload Max 2Mb Format PDF</span>";}else{echo $errorfoto;}?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Piagam</label>
                    <div class="col-sm-5">
                      <input type="file" name="piagam" id="piagam">
                      <p class="help-block"><?php if(empty($error)){echo "<span class='label label-warning'>File Upload Max 2Mb Format PDF, <b>Jika ada Piagam<b></span>";}else{echo $errorfoto;}?></p>
                    </div>
                  </div>                                                      
                </div>

                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Upload</button>
                </div>
              </form>
            </div>
            <!-- /.box -->      
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

