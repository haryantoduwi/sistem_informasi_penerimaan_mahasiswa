 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Pribadi
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Isi Formulir</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <div class="alert alert-info alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> PERHATIAN</h4>
                Isikan data dengan sebenar-benarnya
                <br>
                Format penulisan nama file foto <span class="label label-warning">Nama lengkap diikuti tanggal upload</span>
            </div>           
        </div>
      </div>    
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pendaftaran</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="<?php echo base_url($controller."/simpandatapribadi");?>" method="POST" enctype="multipart/form-data">
                <div class="box-body">
<!--                   <div class="form-group">
                    <label for="">Pilihan Jurusan</label>
                    <select class="form-control select2" style="width: 100%;" name="idjurusan">
                        <?php
                          foreach($jurusan as $dt){
                            echo "<option value=".trim($dt->id).">".ucwords($dt->nama)." - ".ucwords($dt->program)."</option>";
                          }
                        ?>
                    </select>                    
                  </div> -->
                  <div class="form-group">
                    <label for="">NISN</label>
                    <input name="nisn" type="text" class="form-control"  placeholder="">
                  </div>                  
                  <div class="form-group">
                    <label for="">Tanggal Lahir</label>
                    
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>   
                      <input name="tgllahir" type="text" class="form-control pull-right datepicker"  placeholder="Tanggal Lahir">    
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Nomor KTP</label>
                    <input name="noktp" type="text" class="form-control"  placeholder="Nomor KTP">
                  </div>
                  <div class="form-group">
                    <label for="">Jenis Kelamin</label>
                    <select class="form-control select2" name="jeniskelamin" style="width: 100%;">
                      <option selected="selected" value="Laki-laki">Laki-laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Kewarganegaraan</label>
                    <select class="form-control select2" name="kewarganegaraan" style="width: 100%;">
                      <option selected="selected" value="WNI">WNI</option>
                      <option value="WNI Keturunan">WNI Keterunan</option>
                      <option value="WNA">WNA</option>                      
                    </select>                    
                  </div>
                  <div class="form-group">
                    <label for="">Agama</label>
                    <select class="form-control select2" name="agama" style="width: 100%;">
                      <option selected="selected" value="Islam">Islam</option>
                      <option value="Kristen">Kristen</option>
                      <option value="Khatolik">Khatolik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Budha">Budha</option>
                      <option value="Lain-lain">Lain-lain</option>                      
                    </select>                     
                  </div>
<!--                   <div class="form-group">
                    <label for="">Propinsi</label>
                    <input name="propinsi" type="text" class="form-control kapital"  placeholder="Propinsi">
                  </div> -->
                  <div class="form-group">
                    <label>Propinsi</label>
                     <select class="form-control select2" style="width: 100%;" name="propinsi" id="provinsi">
                        <option value="0" selected="selected" disabled="disabled">--Pilih Provinsi--</option>
                        <?php
                          foreach($provinsi as $dt){
                            echo "<option value=".trim($dt->id).">".ucwords($dt->name)."</option>";
                          }
                        ?>
                    </select>                     
                  </div>
                  <div class="form-group">
                    <label for="">Kabupaten</label>
                    <p></p>
                     <!-- <input name="kabupaten" type="text" class="form-control kapital"  placeholder="Kabupaten"> -->
                      <select class="form-control select2" style="width: 100%;" name="kabupaten" id="kabupaten-kota" >
                        <option value="0" selected="selected" disabled="disabled">--Pilih Kabupaten--</option>
                    </select>                       
                  </div>
                  <div class="form-group">
                    <label for="">Kecamatan</label>
                    <!-- <input name="kecamatan" type="text" class="form-control kapital"  placeholder="Kecamatan"> -->
                    <select class="form-control select2" style="width: 100%;" name="kecamatan" id="kecamatan" >
                        <option value="0" selected="selected" disabled="disabled">--Pilih Kabupaten--</option>
                    </select>                   
                  </div>                                    
                  <div class="form-group">
                    <label for="">Alamat Asal</label>
                    <input name="alamatasal" type="text" class="form-control kapital"  placeholder="Alamat Asal">
                  </div>
                  <div class="form-group">
                    <label for="">Kode Pos</label>
                    <input name="kodepos" type="text" class="form-control"  placeholder="Kode Pos">
                  </div>                   
                  <div class="form-group">
                    <label for="">No Handphone</label>
                    <input name="nohp" type="text" class="form-control"  placeholder="No Handphone">
                  </div>
                  <div class="form-group">
                    <label for="">Mengetahui IST AKPRIND dari</label>
                    <select class="form-control select2" name="kuisioner" style="width: 100%;">
                      <option value="Internet">Internet</option>
                      <option value="Keluarga">Keluarga</option>
                      <option value="Teman">Teman</option>
                      <option value="Sekolah/Guru">Sekolah/Guru</option>
                      <option value="Brosure/Leaflet/Poster">Brosure/Leaflet/Poster</option>
                      <option value="Media(Cetak/elektronik)">Media(Cetak/Elektronik)</option>                      
                      <option value="Pameran">Pameran</option>
                      <option value="Lain-lain">Lain-lain</option>                                          
                    </select>                     
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputFile">File Foto</label>
                    <input type="file" name="filefoto" id="exampleInputFile">
                    <p class="help-block"><?php if(empty($errorfoto)){echo "File Upload Max 2Mb Format JPG";}else{echo $errorfoto;}?></p>
                  </div>                                                                                                                                                                                                                                                           
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->      
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

