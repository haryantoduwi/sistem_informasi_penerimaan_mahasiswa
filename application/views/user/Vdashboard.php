 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> INFORMASI</h4>
                Untuk info lebih lanjut mengenai penerimaan mahasiswa baru bisa mengubungi Bagian Informasi
                <br>
                Email   : penmaru@akprind.ac.id
                <br>
                Hunting : (0274) 555032
                <br>
                SMS     : 085713552116
            </div>          
        </div>
      </div>
      <div class="row hide">
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>Status</h3>

                <p>Menunggu Validasi Operator</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>Bounce Rate</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6 hide">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>44</h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="hide col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
      </div>
      <div class="row">
        <div class="col-md-6">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php  if(!empty($dt->filefoto)){ echo base_url("fileupload/foto/".$dt->filefoto);}else{echo base_url('asset/dist/img/avatar6.png');} ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo ucwords($dt->nama);?></h3>

              <p class="text-muted text-center">Calon Mahasiswa Baru</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Nomor Test</b> <a class="pull-right"><?php if(empty($dt->notest)){echo "<span class='label label-info'>Menunggu</span>";}else{echo "<span class='label label-success'>".$dt->notest."</span>";} ?></a>
                </li>               
                <li class="list-group-item">
                  <b>Tanggal Daftar</b> <a class="pull-right"><?php echo date('d-m-Y',strtotime($dt->tgldaftar))?></a>
                </li>                
                <li class="list-group-item">
                  <b>Username/Email</b> <a class="pull-right"><?php echo $dt->email;?></a>
                </li>
                <li class="list-group-item">
                  <b>Password</b> <a class="pull-right"><?php echo ucwords($dt->password);?></a>
                </li>
                <li class="list-group-item">
                  <b>NISN</b> <a class="pull-right"><?php echo ucwords($dt->nisn);?></a>
                </li>
                <li class="list-group-item">
                  <b>No.Tlp</b> <a class="pull-right"><?php echo ucwords($dt->nohandphone);?></a>
                </li>                                
              </ul>
              <?php
                if ($dt->statusvalidasi=='f') {
                  echo "<a href='#' class='btn btn-danger btn-block'><b>Menunggu Validasi</b></a>";
                }else{
                  echo "<a href='#' class='btn btn-success btn-block'><b>Valid</b></a>";
                }
              ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Pribadi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-calendar margin-r-5"></i> Tempat/Tanggal Lahir</strong>

              <p class="text-muted">
               <?php echo ucwords($dt->kabupaten).", " .ucwords(date('d-m-Y',strtotime($dt->tgllahir))) ;?>
              </p>

              <hr>

              <strong><i class="fa fa-credit-card margin-r-5"></i> Nomor KTP</strong>

              <p class="text-muted">
                <?php echo ucwords($dt->noktp);?>
              </p>

              <hr>
              <strong><i class="fa fa-bars margin-r-5"></i> Jenis Kelamin</strong>

              <p class="text-muted">
                <?php echo ucwords($dt->jeniskelamin);?>
              </p>

              <hr>
              <strong><i class="fa fa-bookmark margin-r-5"></i> Kewarganegaraan</strong>

              <p class="text-muted">
                <?php echo ucwords($dt->kewarganegaraan);?>
              </p>
              <hr>
              <strong><i class="fa fa-adjust margin-r-5"></i> Agama</strong>

              <p class="text-muted">
                <?php echo ucwords($dt->agama);?>
              </p>
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat Asal</strong>

              <p class="text-muted">
                <?php echo ucwords($dt->alamat);?>
              </p>
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Kecamatan</strong>

              <p class="text-muted">
                <?php echo ucwords($dt->kecamatan);?>
              </p>
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Propinsi</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->propinsi);?>
              </p>
              <hr>
              <strong><i class="fa fa-envelope margin-r-5"></i> Kode POS</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->kodepos);?>
              </p>
              <hr>
              <strong><i class="fa fa-sticky-note-o margin-r-5"></i> Mengetahui IST AKPRIND Dari</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->kuisioner);?>
              </p>
              <hr>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--DATA ORANGG TUA/WALI-->
        <div class="col-md-6">
          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Orang Tua/Wali</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-user margin-r-5"></i> Nama Ibu </strong>

              <p class="text-muted">
                <?php echo ucwords($dt->namaibu);?>
              </p>

              <hr>

              <strong><i class="fa fa-user margin-r-5"></i> Nama Bapak</strong>

              <p class="text-muted">
                <?php echo ucwords($dt->namabapak);?>
              </p>

              <hr>
              <strong><i class="fa fa-user margin-r-5"></i> Nama Wali</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->namawali);?>
              </p>
 
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Kecamatan</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->kecamatanortu);?>
              </p>
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Kabupaten</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->kabupatenortu);?>
              </p>
              <hr>
              <strong><i class="fa fa-map-marker margin-r-5"></i> Propinsi</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->propinsiortu);?>
              </p>
              <hr>
              <strong><i class="fa fa-envelope margin-r-5"></i> Kode POS</strong>

              <p class="text-muted">
                 <?php echo ucwords($dt->kodeposortu);?>
              </p>
              <hr>
              <strong><i class="fa fa-phone margin-r-5"></i> No. Telp/HP</strong>

              <p class="text-muted"> 
                 <?php echo ucwords($dt->notlportu);?>
              </p> 
            </div>
            <!-- /.box-body -->
          </div>  

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">File Berkas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-download margin-r-5"></i> File Raport </strong>

              <p class="text-muted">
                 <a href="<?php echo base_url('Cuser/downloadfileraport/'.$dt->fileraport)?>"><span class="label label-primary">Download</span></a>  <?php echo ucwords($dt->fileraport);?>
              </p>

              <hr>

              <strong><i class="fa fa-download margin-r-5"></i> File Foto</strong>

              <p class="text-muted">
                <a href="<?php echo base_url('Cuser/downloadfilefoto/'.$dt->filefoto)?>"><span class="label label-primary">Download</span></a>  <?php echo ucwords($dt->filefoto);?> 
              </p>

              <hr>
            </div>
            <!-- /.box-body -->
          </div>                         
        </div>        
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

