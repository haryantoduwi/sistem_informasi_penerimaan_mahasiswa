 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Pribadi
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Isi Formulir</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <div class="alert alert-info alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> PERHATIAN</h4>
                Isikan data dengan sebenar-benarnya
                <br>
                Format penulisan nama file foto <span class="label label-warning">Nama lengkap diikuti tanggal upload</span>
            </div>          
        </div>
      </div>    
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pendaftaran</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="<?php echo base_url($controller."/updatedatapribadi");?>" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group hide">
                    <label for="">Id</label>
                    <input name="id" type="text" class="form-control"  placeholder="" value="<?php echo ucwords($data->id);?>">
                  </div>                
                  <div class="form-group">
                    <label for="">Pilihan Jurusan</label>
                    <select class="form-control select2" style="width: 100%;" name="idjurusan">
                        <?php
                          $res=$data->idjurusan;
                          foreach($jurusan as $dt){
                            ?>
                            <option value="<?php echo trim($dt->id)?>" <?php if(trim($res)==trim($dt->id)){echo 'selected';} ?>><?php echo ucwords($dt->nama)." - ".ucwords($dt->program)?></option>
                            <?php
                          }
                        ?>
                    </select>                    
                  </div>
                  <div class="form-group">
                    <label for="">NISN</label>
                    <input name="nisn" type="text" class="form-control"  placeholder="" value="<?php echo ucwords($data->nisn);?>">
                  </div>                  
                  <div class="form-group">
                    <label for="">Tanggal Lahir</label>
                    
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>   
                      <input name="tgllahir" type="text" class="form-control pull-right datepicker"  placeholder="Tanggal Lahir" value="<?php echo date('d-m-Y',strtotime($data->tgllahir));?>">    
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Nomor KTP</label>
                    <input name="noktp" type="text" class="form-control"  placeholder="Nomor KTP" value="<?php echo ucwords($data->noktp);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Jenis Kelamin</label>
                    <select class="form-control select2" name="jeniskelamin" style="width: 100%;">
                      <option <?php if(trim($data->jeniskelamin)=="Laki-laki") echo "selected";?> value="Laki-laki">Laki-laki</option>
                      <option <?php if(trim($data->jeniskelamin)=="Perempuan") echo "selected";?> value="Perempuan">Perempuan</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Kewarganegaraan</label>
                    <select class="form-control select2" name="kewarganegaraan" style="width: 100%;">
                      <option <?php if(trim($data->kewarganegaraan)=="WNI") echo "selected";?> value="WNI">WNI</option>
                      <option <?php if(trim($data->kewarganegaraan)=="WNI Keturunan") echo "selected";?> value="WNI Keturunan">WNI Keterunan</option>
                      <option <?php if(trim($data->kewarganegaraan)=="WNA") echo "selected";?> value="WNA">WNA</option>                      
                    </select>                    
                  </div>
                  <div class="form-group">
                    <label for="">Agama</label>
                    <select class="form-control select2" name="agama" style="width: 100%;">
                      <option <?php if(trim($data->agama)=="Islam") echo "selected";?> value="Islam">Islam</option>
                      <option <?php if(trim($data->agama)=="Kristen") echo "selected";?> value="Kristen">Kristen</option>
                      <option <?php if(trim($data->agama)=="Khatolik") echo "selected";?> value="Khatolik">Khatolik</option>
                      <option <?php if(trim($data->agama)=="Hindu") echo "selected";?> value="Hindu">Hindu</option>
                      <option <?php if(trim($data->agama)=="Budha") echo "selected";?> value="Budha">Budha</option>
                      <option <?php if(trim($data->agama)=="Lain-lain") echo "selected";?> value="Lain-lain">Lain-lain</option>                      
                    </select>                     
                  </div>
                  <div class="form-group">
                    <label for="">Alamat Asal</label>
                    <input name="alamatasal" type="text" class="form-control kapital"  placeholder="Alamat Asal" value="<?php echo ucwords($data->alamat);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Kecamatan</label>
                    <input name="kecamatan" type="text" class="form-control kapital"  placeholder="Kecamatan" value="<?php echo ucwords($data->kecamatan);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Kabupaten</label>
                    <input name="kabupaten" type="text" class="form-control kapital"  placeholder="Kabupaten" value="<?php echo ucwords($data->kabupaten);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Propinsi</label>
                    <input name="propinsi" type="text" class="form-control kapital"  placeholder="Propinsi" value="<?php echo ucwords($data->propinsi);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Kode Pos</label>
                    <input name="kodepos" type="text" class="form-control"  placeholder="Kode Pos" value="<?php echo ucwords($data->kodepos);?>">
                  </div>                   
                  <div class="form-group">
                    <label for="">No Handphone</label>
                    <input name="nohp" type="text" class="form-control"  placeholder="No Handphone" value="<?php echo ucwords($data->nohandphone);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Mengetahui IST AKPRIND dari</label>
                    <select class="form-control select2" name="kuisioner" style="width: 100%;">
                      <option <?php if(trim($data->kuisioner)=="Internet") echo "selected";?> value="Internet">Internet</option>
                      <option <?php if(trim($data->kuisioner)=="Keluarga") echo "selected";?> value="Keluarga">Keluarga</option>
                      <option <?php if(trim($data->kuisioner)=="Teman") echo "selected";?> value="Teman">Teman</option>
                      <option <?php if(trim($data->kuisioner)=="Sekolah/Guru") echo "selected";?> value="Sekolah/Guru">Sekolah/Guru</option>
                      <option <?php if(trim($data->kuisioner)=="Brosure/Leaflet/Poster") echo "selected";?> value="Brosure/Leaflet/Poster">Brosure/Leaflet/Poster</option>
                      <option <?php if(trim($data->kuisioner)=="Media(Cetak/elektronik)") echo "selected";?> value="Media(Cetak/elektronik)">Media(Cetak/Elektronik)</option>                      
                      <option <?php if(trim($data->kuisioner)=="Pameran") echo "selected";?> value="Pameran">Pameran</option>
                      <option <?php if(trim($data->kuisioner)=="Lain-lain") echo "selected";?> value="Lain-lain">Lain-lain</option>                                          
                    </select>                     
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputFile">File Foto</label>
                    <input type="file" name="filefoto" id="exampleInputFile">
                    <p class="help-block">File Foto : <?php echo $data->filefoto?></p>
                  </div>
                  <div class="form-group hide">
                    <label for="">File Lama</label>
                    <input name="filelama" type="text" class="form-control"  placeholder="file" value="<?php echo $data->filefoto;?>">
                  </div>                                                                                                                                                                                                                                                                             
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.box -->      
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

