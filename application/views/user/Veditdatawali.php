 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit Data Orang Tua/Wali
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Data</li>
      </ol>      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <div class="alert alert-info alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> PERHATIAN</h4>
                Isikan Data Disetiap Kolom Jangan Sampai Ada Yang Dikosongkan.
            </div>          
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pendaftaran</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="<?php echo base_url($controller."/updatedatawali");?>" method="POST">
                <div class="box-body">
                  <div class="form-group hide">
                    <label for="">Idorangtua</label>
                    <input name="id" type="text" class="form-control"  placeholder="Nomor Telepon" value="<?php echo ucwords($data->id);?>">
                  </div>                  
                  <div class="form-group">
                    <label for="">Nama Ibu</label>
                    <input name="namaibu" type="text" class="form-control kapital"  placeholder="Nama Ibu" value="<?php echo ucwords($data->namaibu);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Nama Bapak</label>
                    <input name="namabapak" type="text" class="form-control kapital"  placeholder="Nama Bapak" value="<?php echo ucwords($data->namabapak);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Nama Wali</label>
                    <input name="namawali" type="text" class="form-control kapital"  placeholder="Nama Wali" value="<?php echo ucwords($data->namawali);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Alamat</label>
                    <input name="alamat" type="text" class="form-control kapital"  placeholder="Alamat" value="<?php echo ucwords($data->alamat);?>">
                  </div>                  
                  <div class="form-group">
                    <label for="">Kecamatan</label>
                    <input name="kecamatan" type="text" class="form-control kapital"  placeholder="Kecamatan" value="<?php echo ucwords($data->kecamatan);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Kabupaten</label>
                    <input name="kabupaten" type="text" class="form-control kapital"  placeholder="Kabupaten" value="<?php echo ucwords($data->kabupaten);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Propinsi</label>
                    <input name="propinsi" type="text" class="form-control kapital"  placeholder="Propinsi" value="<?php echo ucwords($data->propinsi);?>">
                  </div>
                  <div class="form-group">
                    <label for="">Kode Pos</label>
                    <input name="kodepos" type="text" class="form-control"  placeholder="Kode POS" value="<?php echo ucwords($data->kodepos);?>">
                  </div>                  
                  <div class="form-group">
                    <label for="">No Tlp/HP Orang Tua</label>
                    <input name="notlp" type="text" class="form-control"  placeholder="Nomor Telepon" value="<?php echo ucwords($data->notlp);?>">
                  </div>                  
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.box -->      
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

