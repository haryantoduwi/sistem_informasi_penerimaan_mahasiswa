<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!--Menu Atas-->
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>PMB</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>PMB</b>AKPRIND</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role=" button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img   src="<?php  if(!empty($dp->filefoto)){ echo base_url("fileupload/foto/".$dp->filefoto);}else{echo base_url('asset/dist/img/avatar6.png');} ?>" class="user-image" alt="User Image">
              <span class="hidden-xs" style="text-transform:capitalize;"><?php echo $this->session->userdata('nama')?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->   
              <li class="user-header">
                <img src="<?php  if(!empty($dp->filefoto)){ echo base_url("fileupload/foto/".$dp->filefoto);}else{echo base_url('asset/dist/img/avatar6.png');} ?>" class="img-circle" alt="User Image">
                <p>
                  <?php echo $this->session->userdata('username');?> 
                  <small class="hide">Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row hide">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url($controller.'/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
              <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
          </li>
        </ul>
      </div>
    </nav>
  </header>