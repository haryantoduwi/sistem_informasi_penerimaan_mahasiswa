  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php  if(!empty($dp->filefoto)){ echo base_url("fileupload/foto/".$dp->filefoto);}else{echo base_url('asset/dist/img/avatar6.png');} ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p style="text-transform:capitalize"><?php echo $this->session->userdata('nama')?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> User-Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo site_url($controller.'/dashboard')?>"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <li class="hide"><a href="<?php echo site_url($controller.'/isiformulir')?>"><i class="fa fa-bar-chart"></i> <span>Isi Formulir</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Isi Formulir</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo site_url($controller.'/isiformulir')?>"><i class="fa fa-user"></i> Data Pribadi</a></li>
            <li class="active"><a href="<?php echo site_url($controller.'/datawali')?>"><i class="fa fa-circle-o"></i> Data Orang Tua/Wali</a></li>
            <li class="active"><a href="<?php echo site_url($controller.'/uploadberkas')?>"><i class="fa fa-cloud-upload"></i> Upload Berkas</a></li>
          </ul>
        </li>
        <li class="hide"><a   href="<?php echo site_url('cuser/lihatdatapegawai')?>"><i class="fa fa-user"></i> <span>Daftar Pegawai</span></a></li>
        <!-- Download Surat Panggilan -->
        <li><a href="<?php echo site_url($controller.'/download')?>"><i class="fa  fa-download"></i> <span>Download</span></a></li>
        <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Panduan</span></a></li>
        <li class="header">LABEL</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
