  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <!--
      <section class="content-header">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>
      -->
      <!-- Main content -->
      <section class="content">
      <!--
        <div class="callout callout-info">
          <h4>Tip!</h4>

          <p>Add the layout-top-nav class to the body tag to get this layout. This feature can also be used with a
            sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular
            links instead.</p>
        </div>
        <div class="callout callout-danger">
          <h4>Warning!</h4>

          <p>The construction of this layout differs from the normal one. In other words, the HTML markup of the navbar
            and the content will slightly differ than that of the normal layout.</p>
        </div>
        -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">PENMARU ONLINE IST AKPRIND</h3>
          </div>
          <div class="box-body">
            <div align="center">
              <img src="./upload/img/PMBOnline.png" class="img-rounded text-center" alt="PMB Online" width="800" height="500" > 
            </div>
            <br>
            <h4><b>Alur Pendaftaran Online</b></h4>
              <ol>
                <li>Mengakses Situs/Web : www.akprind.ac.id</li>
                <li>Pilih Menu Admisi/Penmaru kemudian Isi Data Form Pendaftaran</li>
                <li>Unggah Persyaratan Bebas TES dan Surat Keterangan Dokter Bebas Narkoba dan Buta Warna (Sebagai Rekomendasi)</li>
                <li>Jawaban Sistem Anda Diterima/Surat Panggilan + No. Pendaftaran</li>
                <li>Transfer Pembayaran Tahap 1 Berdasarkan No . Pendaftaran</li>
                <li>Upload File Bukti Pembayaran</li>
                <li>Mendapatkan Kartu Mahasiswa Sementara</li>
              </ol>                    
          </div>         
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->