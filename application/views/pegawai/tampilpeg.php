 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Pegawai
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
    
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Pegawai</h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                <!-- form start -->
                <form role="form" action="cdp3/show" method="POST">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Pangkat</th>
                  <th>Jabatan</th>
                  <th style="text-align:center;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($res as $p){
                  ?>
                    <tr>
                      <td><?php echo $p->nama?></td>
                      <td><?php echo $p->nik?>
                      </td>
                      <td><?php echo $p->pangkat?></td>
                      <td> <?php echo $p->jabatan?></td>
                      <td width="150px" align="center">
                        <a href="#"><span class="label label-warning">Edit</span></a>
                        <a href="#"><span class="label label-danger">Hapus</span></a>
                        <a href="<?php echo base_url('cadmin/mulainilai/'.$p->id)?>"><span class="label label-success">Pilih</span></a>
                      </td>
                    </tr>
                  <?php
                }
                ?>
                </tbody>
              </table>
                </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
           
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->

      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
