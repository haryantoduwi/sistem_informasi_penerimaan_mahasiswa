 
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Pegawai Ke Unit
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
    
            <!-- general form elements -->
            <div class="box box-primary ">
              <div class="box-header with-border">
                <h3 class="box-title">Add Data Pegawai</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="simpanpegawai" method="POST">
                <div class="box-body">
                <div class="form-group hidden">
                  <div class="row">
                    <div class="col-xs-4">
                        <label>Id User</label>
                       <input type="text" readonly class="form-control" id="iduser" name="iduser" value="<?php echo $this->session->userdata('id_user')?>">
                    </div>
                  </div>
                </div>
                <div class="form-group" >
                  <div class="row">
                          <div class="col-sm-4">
                          <label for="kode">Nama Pegawai</label>
                            <input list="nama" name="nama" class="form-control" >
                            <datalist id="nama">
                                 <?php
                                      foreach($pegawai as $master){
                                  ?>
                                      <option value="<?php echo strtoupper($master->employeelastname)." " .strtoupper($master->employeemiddlename)." ".strtoupper($master->employeefirstname);?>"></option>
                                  <?php
                                      }
                                  ?>  
                            </datalist>
                          </div>      
                  </div>                  
                </div>
                <!--PERIODE-->
                <div class="form-group hidden">
                  <label for="kesetiaan">Periode</label>
                  <div class="row ">
                    <div class="col-xs-3">    
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" placeholder="Dari" name="dari" class="form-control pull-right datepicker" >
                          </div>
                    </div>
                    <div class="col-xs-3" >
                       <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" placeholder="sampai" name="sampai" class="form-control pull-right datepicker " >
                          </div>
                    </div>
                    </div>
                  </div>  
                <!--PENILAIAN-->
                <div class="form-group">
                  <label for="">NIK</label>
                  <div class="row ">
                    <div class="col-xs-4">
                      <input type="text" class="form-control" id="NIK" placeholder="NIK Pegawai" name="nik">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="">Pangkat/Golongan Ruang</label>
                  <div class="row ">
                    <div class="col-xs-4">
                      <input type="text" class="form-control" id="" placeholder="" name="pangkat">
                    </div>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="">Jabatan/Staff Kontrak</label>
                  <div class="row ">
                    <div class="col-xs-4">
                      <input type="text" class="form-control" id="" placeholder="" name="jabatan">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="">Unit Organisasi</label>
                  <div class="row ">
                    <div class="col-xs-4">
                      <input type="text" class="form-control" id="" placeholder="" name="unit">
                    </div>
                  </div>
                </div>  
                </div>
                <!-- /.box-body -->
                   <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
             
              </form>
            </div>
            <!-- /.box -->
           
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->

      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
