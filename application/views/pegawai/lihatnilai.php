 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hasil Penilaian
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
    
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Pegawai</h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                <!-- form start -->
                <form role="form" action="cdp3/show" method="POST">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Periode</th>
                  <th>Nilai Total</th>
                  <th>Nilai Rata-rata</th>
                  <th style="text-align:center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($dp3 as $p){
                  ?>
                    <tr>
                      <td><?php echo strtoupper($p->nik)?></td>
                      <td><?php echo strtoupper($p->nama)?></td>
                      <td><?php echo $p->periodeawal?> - <?php echo $p->periodeakhir?> </td>
                      <?php $total=$p->kesetiaan + $p->prestasi + $p->tanggungjwb + $p->ketaatan + $p->kejujuran + $p->kerjasama + $p->prakarsa + $p->kepemimpinan;
                      $res=$total/8;
                      ?>
                      <td><?php echo $total;?></td>
                      <td><?php echo number_format($res,2)?></td>
                      <td width="200px" align="center">
                        <a href="#" class="edit" id="<?php echo  $p->id_penilaian; ?>"><span class="label label-warning">Edit</span></a>
                        <a href="<?php echo base_url('cadmin/hapusdp3/'.$p->id_penilaian);?>"><span class="label label-danger">Hapus</span></a>                      
                        <a href="<?php echo base_url('cadmin/penilaian/'.$p->id_penilaian);?>"><span class="label label-success"> Pilih</span></a>
                        <a href="#"><span class="label label-success"> <i class="fa fa-print"></i></span></a>  
                      </td>
                    </tr>
                  <?php
                }
                ?>

                </tbody>
              </table>
                </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
           
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->

      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
