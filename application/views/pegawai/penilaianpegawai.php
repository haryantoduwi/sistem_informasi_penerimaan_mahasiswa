 
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penilaian DP3
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
    
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Penilaian DP3</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="<?php echo base_url('cadmin/simpanpenilaian');?>" method="POST">
                <div class="box-body">
                <div class="form-group hidden">
                  <div class="row">
                    <div class="col-xs-6">
                        <label>Id Pegawai</label>
                       <input type="text" class="form-control" readonly name="idpegawai" value="<?php echo $pegawai->id;?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-xs-6">
                        <label>Nama</label>
                       <input type="text" class="form-control" readonly name="nama" value="<?php echo $pegawai->nama;?>">
                    </div>
                  </div>
                </div>
                <!--PERIODE-->
                <div class="form-group">
                  <label for="kesetiaan">Periode</label>
                  <div class="row ">
                    <div class="col-xs-3">    
                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" placeholder="Dari" name="periodeawal" class="form-control pull-right datepicker" >
                          </div>
                    </div>
                    <div class="col-xs-3" >
                       <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" placeholder="sampai" name="periodeakhir" class="form-control pull-right datepicker " >
                          </div>
                    </div>
                    </div>
                  </div>  
                <!--PENILAIAN-->
                <div class="form-group">
                  <label for="kesetiaan">Kesetiaan</label>
                  <div class="row ">
                    <div class="col-xs-3">
                      <input type="number" onchange="hitung()" class="form-control" id="kesetiaan" placeholder="Kesetiaan" name="kesetiaan" min="1" max="100" value="0">
                    </div>
                    <div class="col-xs-3" >
                     <input type="text" readonly class="form-control" id="kesetiaanI" placeholder="Kesetiaan" name="kesetiaanI" value="kurang">
                    </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="prestasikerja">Prestasi Kerja</label>
                  <div class="row">
                    <div class="col-xs-3">
                      <input type="number"  onchange="hitung()" class="form-control" id="prestasikerja" placeholder="Prestasi Kerja" name="prestasikerja" min="1" max="100" value="0">
                    </div>
                    <div class="col-xs-3">
                       <input type="text" readonly class="form-control" id="prestasiI" name="prestasiI" value="Kurang">
                    </div>
                  </div>
                  </div>
                  <div class="form-group ">
                    <label for="tanggungjawab">Tanggung Jawab</label>
                  <div class="row"> 
                    <div class="col-xs-3">
                      <input type="number"  onchange="hitung()" class="form-control" id="tanggungjawab" placeholder="Tanggungjawab" name="tanggungjawab" min="1" max="100" value="0">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" readonly class="form-control" id="tanggungjawabI"  name="tanggungjawabI" value="Kurang">
                    </div>
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="ketaatan">Ketaatan</label>
                  <div class="row">
                    <div class="col-xs-3">
                      <input type="number"  onchange="hitung()" class="form-control" id="ketaatan" placeholder="Ketaatan" name="ketaatan" min="1" max="100" value="0">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" readonly class="form-control" id="ketaatanI"  name="ketaatanI" value="Kurang">
                    </div>
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="kejujuran">Kejujuran</label>
                      <div class="row">
                        <div class="col-xs-3">
                          <input type="number"  onchange="hitung()" class="form-control" id="kejujuran" placeholder="Kejujuran" name="kejujuran" min="1" max="100" value="0">
                        </div>
                        <div class="col-xs-3">
                          <input type="text" readonly class="form-control" id="kejujuranI"  name="kejujuranI" value="Kurang">
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                      <label for="kerjasama">Kerjasama</label>
                        <div class="row">
                          <div class="col-xs-3">
                               <input type="number"  onchange="hitung()" class="form-control" id="kerjasama" placeholder="Kerjasama" name="kerjasama" min="1" max="100" value="0">
                          </div>
                          <div class="col-xs-3">
                            <input type="text" readonly class="form-control" id="kerjasamaI"  name="kerjasamaI" value="Kurang">
                          </div>
                        </div>
                  </div>
                  <div class="form-group">
                    <label for="prakarsa">Prakarsa</label>
                      <div class="row">
                        <div class="col-xs-3">
                          <input type="number"  onchange="hitung()" class="form-control" id="prakarsa" placeholder="Prakarsa" name="prakarsa" min="1" max="100" value="0">
                        </div>
                        <div class="col-xs-3">
                          <input type="text" readonly class="form-control" id="prakarsaI"  name="prakarsaI" value="Kurang">
                        </div>
                      </div>
                    
                  </div>
                  <div class="form-group">
                    <label for="kepemimpinan">Kepemimpinan</label>
                    <div class="row">
                      <div class="col-xs-3">
                        <input type="number"  onchange="hitung()" class="form-control" id="kepemimpinan" placeholder="Kepemimpinan" name="kepemimpinan" min="1" max="100" value="0">
                      </div>
                      <div class="col-xs-3">
                        <input type="text" readonly class="form-control" id="kepemimpinanI"  name="kepemimpinanI" value="Kurang">            
                      </div>
                    </div>
                    
                  </div>
                  <div class="form-group hidden" >
                      <div class="row">
                        <div class="col-xs-6">
                             <button type="button" id="tombol_p" class="btn btn-primary">Hitung</button>
                        </div> 
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <div class="row">
                      <div class="col-xs-6"> 
                        <input type="text" readonly class="form-control" id="jumlah" placeholder="Jumlah" name="jumlah">
                      </div>
                    </div>
                    
                  </div>
                  <div class="form-group">
                    <label for="ratarata">Rata-rata</label>
                      <div class="row">
                        <div class="col-xs-6">
                           <input readonly type="text" class="form-control" id="ratarata" placeholder="Nila Rata-rata" name="ratarata"> 
                        </div>
                      </div>
                  </div>
                </div>
                <!-- /.box-body -->
                   <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
             
              </form>
            </div>
            <!-- /.box -->
           
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->

      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
    document.getElementById("tombol_p").
    addEventListener("click", hitung);
    
    function hitung() {
    var nilai_kesetiaan=parseInt(document.getElementById("kesetiaan").value);
    var nilai_prestasi=parseInt(document.getElementById("prestasikerja").value);
    var nilai_tanggungjawab=parseInt(document.getElementById("tanggungjawab").value);
    var nilai_ketaatan=parseInt(document.getElementById("ketaatan").value);
    var nilai_kejujuran=parseInt(document.getElementById("kejujuran").value);
    var nilai_kerjasama=parseInt(document.getElementById("kerjasama").value);
    var nilai_prakarsa=parseInt(document.getElementById("prakarsa").value);
    var nilai_kepemimpinan=parseInt(document.getElementById("kepemimpinan").value);
    var jumlah=nilai_kesetiaan+nilai_prestasi+nilai_tanggungjawab+nilai_ketaatan+nilai_kejujuran+nilai_kerjasama+nilai_prakarsa+nilai_kepemimpinan;
    //alert(jumlah);
      if(nilai_kesetiaan>=90){
        document.getElementById("kesetiaanI").value="Amat Baik";
      }else if(nilai_kesetiaan>=70){
        document.getElementById("kesetiaanI").value="Baik";
      }else if(nilai_kesetiaan>=50){
         document.getElementById("kesetiaanI").value="Cukup";
      }else{
         document.getElementById("kesetiaanI").value="Kurang";
      }
      if(nilai_prestasi>=90){
        document.getElementById("prestasiI").value="Amat Baik";
      }else if(nilai_prestasi>=70){
        document.getElementById("prestasiI").value="Baik";
      }else if(nilai_prestasi>=50){
         document.getElementById("prestasiI").value="Cukup";
      }else{
         document.getElementById("prestasiI").value="Kurang";
      }
      if(nilai_tanggungjawab>=90){
        document.getElementById("tanggungjawabI").value="Amat Baik";
      }else if(nilai_tanggungjawab>=70){
        document.getElementById("tanggungjawabI").value="Baik";
      }else if(nilai_tanggungjawab>=50){
         document.getElementById("tanggungjawabI").value="Cukup";
      }else{
         document.getElementById("tanggungjawabI").value="Kurang";
      }
      if(nilai_ketaatan>=90){
        document.getElementById("ketaatanI").value="Amat Baik";
      }else if(nilai_ketaatan>=70){
        document.getElementById("ketaatanI").value="Baik";
      }else if(nilai_ketaatan>=50){
         document.getElementById("ketaatanI").value="Cukup";
      }else{
         document.getElementById("ketaatanI").value="Kurang";
      }
      if(nilai_kejujuran>=90){
        document.getElementById("kejujuranI").value="Amat Baik";
      }else if(nilai_kejujuran>=70){
        document.getElementById("kejujuranI").value="Baik";
      }else if(nilai_kejujuran>=50){
         document.getElementById("kejujuranI").value="Cukup";
      }else{
         document.getElementById("kejujuranI").value="Kurang";
      }
      if(nilai_kerjasama>=90){
        document.getElementById("kerjasamaI").value="Amat Baik";
      }else if(nilai_kerjasama>=70){
        document.getElementById("kerjasamaI").value="Baik";
      }else if(nilai_kerjasama>=50){
         document.getElementById("kerjasamaI").value="Cukup";
      }else{
         document.getElementById("kerjasamaI").value="Kurang";
      }
      if(nilai_prakarsa>=90){
        document.getElementById("prakarsaI").value="Amat Baik";
      }else if(nilai_prakarsa>=70){
        document.getElementById("prakarsaI").value="Baik";
      }else if(nilai_prakarsa>=50){
         document.getElementById("prakarsaI").value="Cukup";
      }else{
         document.getElementById("prakarsaI").value="Kurang";
      }
      if(nilai_kepemimpinan>=90){
        document.getElementById("kepemimpinanI").value="Amat Baik";
      }else if(nilai_kepemimpinan>=70){
        document.getElementById("kepemimpinanI").value="Baik";
      }else if(nilai_kepemimpinan>=50){
         document.getElementById("kepemimpinanI").value="Cukup";
      }else{
         document.getElementById("kepemimpinanI").value="Kurang";
      }

      document.getElementById("jumlah").value=jumlah;
      var rata2 = jumlah/8;
      document.getElementById("ratarata").value=rata2;
    }
</script>
