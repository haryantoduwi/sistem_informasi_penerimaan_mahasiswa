 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Pegawai
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
    
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Pegawai</h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                <!-- form start -->
                <form role="form" action="cdp3/show" method="POST">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Jenis Kelamin</th>
                  <th>Alamat</th>
                  <th>Id Fingerprind</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($pegawai as $p){
                  ?>
                    <tr>
                      <td><?php echo $p->nama?></td>
                      <td><?php echo $p->nik?>
                      </td>
                      <td><?php echo $p->idjeniskelamin?></td>
                      <td> <?php echo $p->alamatjalan?></td>
                      <td><?php echo $p->fingerprintid?></td>
                    </tr>
                  <?php
                }
                ?>
               
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Jenis Kelamin</th>
                  <th>Alamat</th>
                  <th>Id Fingerprind</th>
                </tr>
                </tfoot>
              </table>
                </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
           
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->

      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
