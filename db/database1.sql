/*
Navicat PGSQL Data Transfer

Source Server         : Localhost
Source Server Version : 90303
Source Host           : localhost:5432
Source Database       : pmb_db
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90303
File Encoding         : 65001

Date: 2017-04-23 20:39:50
*/


-- ----------------------------
-- Sequence structure for tb_administrator_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tb_administrator_id_seq";
CREATE SEQUENCE "public"."tb_administrator_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."tb_administrator_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for tb_datadiri_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tb_datadiri_id_seq";
CREATE SEQUENCE "public"."tb_datadiri_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."tb_datadiri_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for tb_file_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tb_file_id_seq";
CREATE SEQUENCE "public"."tb_file_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for tb_jurusan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tb_jurusan_id_seq";
CREATE SEQUENCE "public"."tb_jurusan_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 13
 CACHE 1;
SELECT setval('"public"."tb_jurusan_id_seq"', 13, true);

-- ----------------------------
-- Sequence structure for tb_orangtua_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tb_orangtua_id_seq";
CREATE SEQUENCE "public"."tb_orangtua_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."tb_orangtua_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for tb_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tb_user_id_seq";
CREATE SEQUENCE "public"."tb_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."tb_user_id_seq"', 2, true);

-- ----------------------------
-- Table structure for tb_administrator
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_administrator";
CREATE TABLE "public"."tb_administrator" (
"id" int4 DEFAULT nextval('tb_administrator_id_seq'::regclass) NOT NULL,
"nama" varchar(100) COLLATE "default",
"username" varchar(100) COLLATE "default",
"password" varchar(255) COLLATE "default",
"level" bool
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_administrator
-- ----------------------------
INSERT INTO "public"."tb_administrator" VALUES ('1', 'p3mb', 'p3mb', 'p3mb', 't');

-- ----------------------------
-- Table structure for tb_datadiri
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_datadiri";
CREATE TABLE "public"."tb_datadiri" (
"iduser" int4,
"idjurusan" int4,
"tgllahir" date,
"jeniskelamin" varchar(100) COLLATE "default",
"kewarganegaraan" varchar(100) COLLATE "default",
"agama" varchar(100) COLLATE "default",
"alamat" text COLLATE "default",
"kecamatan" text COLLATE "default",
"propinsi" text COLLATE "default",
"kodepos" varchar(50) COLLATE "default",
"nohandphone" varchar(20) COLLATE "default",
"nisn" varchar(100) COLLATE "default",
"kabupaten" text COLLATE "default",
"tgldaftar" date,
"statusvalidasi" bool,
"notest" varchar(100) COLLATE "default",
"noktp" varchar(50) COLLATE "default",
"id" int2 DEFAULT nextval('tb_datadiri_id_seq'::regclass) NOT NULL,
"kuisioner" text COLLATE "default",
"filefoto" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_datadiri
-- ----------------------------
INSERT INTO "public"."tb_datadiri" VALUES ('1', '5', '1991-07-02', 'Laki-laki', 'WNI', 'Islam', 'pedak wijirejo pandak bantul', 'pandak', 'yogyakarta', '55761', '0867676676767', '989089899', 'bantul', '2017-04-19', 'f', null, '8908980899898', '1', 'Internet', null);

-- ----------------------------
-- Table structure for tb_file
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_file";
CREATE TABLE "public"."tb_file" (
"iduser" int4,
"fileraport" text COLLATE "default",
"filefoto" text COLLATE "default",
"id" int2 DEFAULT nextval('tb_file_id_seq'::regclass) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_file
-- ----------------------------

-- ----------------------------
-- Table structure for tb_jurusan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_jurusan";
CREATE TABLE "public"."tb_jurusan" (
"id" int4 DEFAULT nextval('tb_jurusan_id_seq'::regclass) NOT NULL,
"kode" varchar(10) COLLATE "default",
"nama" varchar(255) COLLATE "default",
"program" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_jurusan
-- ----------------------------
INSERT INTO "public"."tb_jurusan" VALUES ('1', '01', 'Teknik Kimia', 'Strata 1');
INSERT INTO "public"."tb_jurusan" VALUES ('2', '02 ', 'Teknik Industri', 'Strata 1');
INSERT INTO "public"."tb_jurusan" VALUES ('3', '03', 'Teknik Mesin', 'Srtara 1');
INSERT INTO "public"."tb_jurusan" VALUES ('4', '04', 'Teknik Elektro', 'Stara 1');
INSERT INTO "public"."tb_jurusan" VALUES ('5', '05', 'Teknik Informatika', 'Starat 1');
INSERT INTO "public"."tb_jurusan" VALUES ('6', '06', 'Statistika', 'Strata 1');
INSERT INTO "public"."tb_jurusan" VALUES ('7', '07', 'Sistem Komputer', 'Strata 1');
INSERT INTO "public"."tb_jurusan" VALUES ('8', '10', 'Teknik Geologi', 'Strata 1');
INSERT INTO "public"."tb_jurusan" VALUES ('9', '11', 'Teknik Lingkungan', 'Strata 1');
INSERT INTO "public"."tb_jurusan" VALUES ('10', '32', 'Teknik Industri', 'Diploma 3');
INSERT INTO "public"."tb_jurusan" VALUES ('11', '33', 'Teknik Mesin', 'Diploma 3');
INSERT INTO "public"."tb_jurusan" VALUES ('12', '34', 'Teknik Elektronika', 'Diploma 3');
INSERT INTO "public"."tb_jurusan" VALUES ('13', '35', 'Manajemen Informatika dan Teknik Komputer', 'Diploma 3');

-- ----------------------------
-- Table structure for tb_orangtua
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_orangtua";
CREATE TABLE "public"."tb_orangtua" (
"iduser" int4,
"namaibu" varchar(100) COLLATE "default",
"namabapak" varchar(100) COLLATE "default",
"alamat" text COLLATE "default",
"kecamatan" text COLLATE "default",
"propinsi" text COLLATE "default",
"kodepos" varchar(20) COLLATE "default",
"notlp" varchar(20) COLLATE "default",
"kabupaten" text COLLATE "default",
"namawali" varchar(100) COLLATE "default",
"id" int2 DEFAULT nextval('tb_orangtua_id_seq'::regclass) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_orangtua
-- ----------------------------
INSERT INTO "public"."tb_orangtua" VALUES ('1', 'Sarjiyem', 'murdiyana', 'pedak wijirejo pandak bantul', 'pandak', 'yogyakarta', '55761', '0857252222', 'bantul', 'sarjiyem', '2');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_user";
CREATE TABLE "public"."tb_user" (
"id" int4 DEFAULT nextval('tb_user_id_seq'::regclass) NOT NULL,
"nama" varchar(200) COLLATE "default",
"email" varchar(100) COLLATE "default",
"password" varchar(255) COLLATE "default",
"level" bool
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO "public"."tb_user" VALUES ('1', 'duwi haryanto', 'haryanto.duwi@gmail.com', 'z3r0nul3', 'f');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."tb_administrator_id_seq" OWNED BY "tb_administrator"."id";
ALTER SEQUENCE "public"."tb_datadiri_id_seq" OWNED BY "tb_datadiri"."id";
ALTER SEQUENCE "public"."tb_file_id_seq" OWNED BY "tb_file"."id";
ALTER SEQUENCE "public"."tb_jurusan_id_seq" OWNED BY "tb_jurusan"."id";
ALTER SEQUENCE "public"."tb_orangtua_id_seq" OWNED BY "tb_orangtua"."id";
ALTER SEQUENCE "public"."tb_user_id_seq" OWNED BY "tb_user"."id";

-- ----------------------------
-- Primary Key structure for table tb_administrator
-- ----------------------------
ALTER TABLE "public"."tb_administrator" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tb_datadiri
-- ----------------------------
ALTER TABLE "public"."tb_datadiri" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tb_file
-- ----------------------------
ALTER TABLE "public"."tb_file" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tb_jurusan
-- ----------------------------
ALTER TABLE "public"."tb_jurusan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tb_orangtua
-- ----------------------------
ALTER TABLE "public"."tb_orangtua" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tb_user
-- ----------------------------
ALTER TABLE "public"."tb_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."tb_datadiri"
-- ----------------------------
ALTER TABLE "public"."tb_datadiri" ADD FOREIGN KEY ("idjurusan") REFERENCES "public"."tb_jurusan" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."tb_datadiri" ADD FOREIGN KEY ("iduser") REFERENCES "public"."tb_user" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."tb_file"
-- ----------------------------
ALTER TABLE "public"."tb_file" ADD FOREIGN KEY ("iduser") REFERENCES "public"."tb_user" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."tb_orangtua"
-- ----------------------------
ALTER TABLE "public"."tb_orangtua" ADD FOREIGN KEY ("iduser") REFERENCES "public"."tb_user" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
