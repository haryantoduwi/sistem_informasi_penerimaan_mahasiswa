/*
Navicat PGSQL Data Transfer

Source Server         : PostGre
Source Server Version : 90303
Source Host           : localhost:5432
Source Database       : penmaru
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90303
File Encoding         : 65001

Date: 2017-06-07 14:02:09
*/


-- ----------------------------
-- Sequence structure for administrator_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."administrator_id_seq";
CREATE SEQUENCE "public"."administrator_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."administrator_id_seq"', 1, true);

-- ----------------------------
-- Table structure for tb_administrator
-- ----------------------------
DROP TABLE IF EXISTS "public"."tb_administrator";
CREATE TABLE "public"."tb_administrator" (
"id" int2 DEFAULT nextval('administrator_id_seq'::regclass) NOT NULL,
"nama" varchar(100) COLLATE "default" NOT NULL,
"username" varchar(100) COLLATE "default" NOT NULL,
"password" varchar(255) COLLATE "default" NOT NULL,
"level" varchar(100) COLLATE "default" NOT NULL,
"tgl" date NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tb_administrator
-- ----------------------------
INSERT INTO "public"."tb_administrator" VALUES ('1', 'duwi', 'admin', 'admin', 'administrator', '2017-05-19');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."administrator_id_seq" OWNED BY "tb_administrator"."id";

-- ----------------------------
-- Primary Key structure for table tb_administrator
-- ----------------------------
ALTER TABLE "public"."tb_administrator" ADD PRIMARY KEY ("id");
